# B2 Réseau 2022 - TP1

LAROUMANIE Gabriel - B2 classe B en Informatique

# TP1 - Mise en jambes

# Sommaire
- [B2 Réseau 2022 - TP1](#b2-réseau-2022---tp1)
- [TP1 - Mise en jambes](#tp1---mise-en-jambes)
- [Sommaire](#sommaire)
- [I. Exploration locale en solo](#i-exploration-locale-en-solo)
  - [1. Affichage d'informations sur la pile TCP/IP locale](#1-affichage-dinformations-sur-la-pile-tcpip-locale)
    - [En ligne de commande](#en-ligne-de-commande)
    - [En graphique (GUI : Graphical User Interface)](#en-graphique-gui--graphical-user-interface)
    - [Questions](#questions)
  - [2. Modifications des informations](#2-modifications-des-informations)
    - [A. Modification d'adresse IP (part 1)](#a-modification-dadresse-ip-part-1)
- [II. Exploration locale en duo](#ii-exploration-locale-en-duo)
  - [1. Prérequis](#1-prérequis)
  - [2. Câblage](#2-câblage)
  - [Création du réseau (oupa)](#création-du-réseau-oupa)
  - [3. Modification d'adresse IP](#3-modification-dadresse-ip)
  - [4. Utilisation d'un des deux comme gateway](#4-utilisation-dun-des-deux-comme-gateway)
  - [5. Petit chat privé](#5-petit-chat-privé)
  - [6. Firewall](#6-firewall)
- [III. Manipulations d'autres outils/protocoles côté client](#iii-manipulations-dautres-outilsprotocoles-côté-client)
  - [1. DHCP](#1-dhcp)
  - [2. DNS](#2-dns)
- [IV. Wireshark](#iv-wireshark)


# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

En utilisant la ligne de commande (CLI) de votre OS :

**🌞 Affichez les infos des cartes réseau de votre PC**

**Interface Wifi**

```sh
[gaby@Void ~]$ ip a | head -n 14 | tail -n 6
3: wlp0s20f3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 8c:8d:28:c8:3b:56 brd ff:ff:ff:ff:ff:ff
    inet 10.33.19.119/22 brd 10.33.19.255 scope global dynamic noprefixroute wlp0s20f3
       valid_lft 84002sec preferred_lft 84002sec
    inet6 fe80::a364:39b2:319c:f711/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```

**Interface Ethernet**

```sh
[gaby@Void ~]$ ip a | head -n 8 | tail -n 2
2: enp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 08:97:98:c8:5b:2b brd ff:ff:ff:ff:ff:ff
```

**🌞 Affichez votre gateway**

```sh
[gaby@Void ~]$ ip r s | grep default
default via 10.33.19.254 dev wlp0s20f3 proto dhcp src 10.33.19.119 metric 600 
```

### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS :  

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

- trouvez l'IP, la MAC et la gateway pour l'interface WiFi de votre PC

![GUI_Interface](./Images/gateway.png)

### Questions

- 🌞 à quoi sert la gateway dans le réseau d'YNOV ?

La gateway dans le réseau d'YNOV permet de connecter les réseaux locaux aux réseaux Internets

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

- changez l'adresse IP de votre carte WiFi pour une autre
- ne changez que le dernier octet
  - par exemple pour `10.33.1.10`, ne changez que le `10`
  - valeur entre 1 et 254 compris

![Ip_Static](./Images/IP-STATIC.png)

🌞 **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

**On peut perdre l'accès a Internet car si un ordinateur à déjà cette ip, les paquets sont donc envoyés au premier détenteur**

# II. Exploration locale en duo

Travail effectué en groupe avec VASSEUR Alexis, ROULLAND Roxanne, GILLES Maxence, GARCIA LUKA

### 1. Prérequis

Lu.

### 2. Câblage

Fait.

### 3. Modification d'adresse IP

![change](./Images/change192168.png)

![bischange](./Images/bischange192168.png)

```bash
# vérification des changements sur le premier ordinateur
C:\Users\Matteo>ipconfig
Configuration IP de Windows
Carte Ethernet Ethernet :
   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::e1db:4bc1:6c23:6d5f%10
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.137.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.137.1
```

```bash
# vérification des changements sur le deuxième ordinateur
Ethernet adapter Ethernet:
   Connection-specific DNS Suffix  . :
   Link-local IPv6 Address . . . . . : fe80::dd19:ecee:b04e:a3e7%22
   IPv4 Address. . . . . . . . . . . : 192.168.137.1
   Subnet Mask . . . . . . . . . . . : 255.255.255.252
```   

```bash
# ping du PC1 vers le PC2
PS C:\Users\Dreasy> ping 192.168.137.2
Pinging 192.168.137.2 with 32 bytes of data:
Reply from 192.168.137.2: bytes=32 time<1ms TTL=128
Reply from 192.168.137.2: bytes=32 time=2ms TTL=128
Reply from 192.168.137.2: bytes=32 time=1ms TTL=128
Reply from 192.168.137.2: bytes=32 time=1ms TTL=128
Ping statistics for 192.168.137.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 2ms, Average = 1ms
```

```bash
# ping du PC2 vers le PC1
C:\Users\Matteo>ping 192.168.137.1
Envoi d’une requête 'Ping'  192.168.137.1 avec 32 octets de données :
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128
Statistiques Ping pour 192.168.137.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```

```bash
# affichage et consultation de la table ARP du premier ordinateur
Interface: 192.168.137.1 --- 0x16
  Internet Address      Physical Address      Type
  192.168.137.2           04-d4-c4-e6-15-34     dynamic
  192.168.137.3           ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
```

```bash
# affichage et consultation de la table ARP du second ordinateur
C:\Users\Matteo>arp -a
[...]
Interface : 192.168.137.2 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.137.1           a0-ce-c8-38-0c-17     dynamique
  192.168.137.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

## 4. Utilisation d'un des deux comme gateway

```bash
#requête vers un serveur connu
C:\Users\Matteo>ping 1.1.1.1
Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=24 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=21 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=54
Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 21ms, Maximum = 24ms, Moyenne = 22ms
```

```bash
# vérification du passage des paquets par la passerelle
C:\Users\Matteo>tracert 1.1.1.1
Détermination de l’itinéraire vers one.one.one.one [1.1.1.1]
avec un maximum de 30 sauts :
  1    <1 ms     *        2 ms  DREASY [192.168.137.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     5 ms     5 ms     5 ms  10.33.19.254
  4     6 ms     6 ms     6 ms  137.149.196.77.rev.sfr.net [77.196.149.137]
  5    14 ms    12 ms    12 ms  108.97.30.212.rev.sfr.net [212.30.97.108]
```

## 5. Petit chat privé

```bash
# informations visibles depuis le pc serveur
PS C:\Users\Dreasy> ncat -l -p 8888
Coucou
Miew
g fAIm
```

```bash
# informations visibles depuis le pc client
PS C:\Users\Matteo> ncat  192.168.137.1 8888
libnsock ssl_init_helper(): OpenSSL legacy provider failed to load.
Coucou
Miew
g fAIm
```

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

🌞Exploration du DHCP, depuis votre PC

- afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
- cette adresse a une durée de vie limitée. C'est le principe du ***bail DHCP*** (ou *DHCP lease*). Trouver la date d'expiration de votre bail DHCP
- vous pouvez vous renseigner un peu sur le fonctionnement de DHCP dans les grandes lignes. On aura sûrement un cours là dessus :)


Création du fichier dhcp car non présent dans l'ordinateur
```sh
[gaby@Void ~]$ sudo dhclient -d -nw wlp0s20f3
Internet Systems Consortium DHCP Client 4.4.3
Copyright 2004-2022 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/wlp0s20f3/8c:8d:28:c8:3b:56
Sending on   LPF/wlp0s20f3/8c:8d:28:c8:3b:56
Sending on   Socket/fallback
DHCPDISCOVER on wlp0s20f3 to 255.255.255.255 port 67 interval 3
DHCPOFFER of 10.33.18.148 from 10.33.19.254
DHCPREQUEST for 10.33.18.148 on wlp0s20f3 to 255.255.255.255 port 67
DHCPACK of 10.33.18.148 from 10.33.19.254
RTNETLINK answers: File exists
bound to 10.33.18.148 -- renewal in 37020 seconds.
```

Ip du serveur DHCP du réseau WiFi Ynov
```sh
[gaby@Void ~]$ sudo less /var/lib/dhclient/dhclient.leases | grep "identifier"
  option dhcp-server-identifier 10.33.19.254;
```

Date d'expiration du bail DHCP
```sh
[gaby@Void ~]$ sudo less /var/lib/dhclient/dhclient.leases | grep "expire"
  expire 4 2022/09/29 09:43:00;
```



## 2. DNS

- 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```sh
[gaby@Void ~]$ cat /etc/resolv.conf
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
```

- 🌞 utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

  - faites un *lookup* (*lookup* = "dis moi à quelle IP se trouve tel nom de domaine")
    - pour `google.com`
    - pour `ynov.com`
    - interpréter les résultats de ces commandes
  - déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes
  - faites un *reverse lookup* (= "dis moi si tu connais un nom de domaine pour telle IP")
    - pour l'adresse `78.74.21.21`
    - pour l'adresse `92.146.54.88`
    - interpréter les résultats
    - *si vous vous demandez, j'ai pris des adresses random :)*


## lookup
```sh
[gaby@Void ~]$ dig google.com
; <<>> DiG 9.18.6 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 50246
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		125	IN	A	142.250.179.110

;; Query time: 16 msec
;; SERVER: 192.168.0.254#53(192.168.0.254) (UDP)
;; WHEN: Wed Sep 28 21:57:42 CEST 2022
;; MSG SIZE  rcvd: 55
```

```sh
[gaby@Void ~]$ dig ynov.com
; <<>> DiG 9.18.6 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23287
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;ynov.com.			IN	A

;; ANSWER SECTION:
ynov.com.		300	IN	A	104.26.10.233
ynov.com.		300	IN	A	104.26.11.233
ynov.com.		300	IN	A	172.67.74.226

;; Query time: 23 msec
;; SERVER: 192.168.0.254#53(192.168.0.254) (UDP)
;; WHEN: Wed Sep 28 22:03:40 CEST 2022
;; MSG SIZE  rcvd: 85
```
### Interprétation des résultats

On lance une requête DNS sur le serveur 192.168.0.254 qui nous renvoie les adresses ip liés aux noms de domaine

## Reverse Proxy 

```sh
[gaby@Void ~]$ dig -x 78.74.21.21

; <<>> DiG 9.18.6 <<>> -x 78.74.21.21
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 2622
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;21.21.74.78.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
21.21.74.78.in-addr.arpa. 3600	IN	PTR	host-78-74-21-21.homerun.telia.com.

;; Query time: 276 msec
;; SERVER: 192.168.0.254#53(192.168.0.254) (UDP)
;; WHEN: Wed Sep 28 23:16:49 CEST 2022
;; MSG SIZE  rcvd: 101

```

```sh
[gaby@Void ~]$ dig -x 92.146.54.88

; <<>> DiG 9.18.6 <<>> -x 92.146.54.88
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 14711
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;88.54.146.92.in-addr.arpa.	IN	PTR

;; AUTHORITY SECTION:
92.in-addr.arpa.	642	IN	SOA	pri.authdns.ripe.net. dns.ripe.net. 1664384863 3600 600 864000 3600

;; Query time: 33 msec
;; SERVER: 192.168.0.254#53(192.168.0.254) (UDP)
;; WHEN: Wed Sep 28 23:16:28 CEST 2022
;; MSG SIZE  rcvd: 114

```

### Interprétation des résultats

On peut voir si les adresses ip possèdent un nom de domaine

# IV. Wireshark

- téléchargez l'outil Wireshark
```sh
[gaby@Void ~]$ sudo pacman -S wireshark-qt
```

- 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
  - un `ping` entre vous et la passerelle
  - un `netcat` entre vous et votre mate, branché en RJ45
  - une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
  - prenez moi des screens des trames en question
  - on va prendre l'habitude d'utiliser Wireshark souvent dans les cours, pour visualiser ce qu'il se passe

