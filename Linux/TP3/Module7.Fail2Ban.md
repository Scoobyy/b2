# Module 7 : Fail2Ban

## Intro

**Nous allons mettre en place un fail2ban sur toutes les machines ayant un service réseau susceptible de se faire bruteforce**

- Installation de Fail2Ban

```bash
[scooby@proxy ~]$ sudo dnf install fail2ban -y
Last metadata expiration check: 2:17:00 ago on Thu Nov 24 00:19:16 2022.
Dependencies resolved.
[...]
Installed:
  esmtp-1.2-19.el9.x86_64                            fail2ban-1.0.1-2.el9.noarch
  fail2ban-firewalld-1.0.1-2.el9.noarch              fail2ban-sendmail-1.0.1-2.el9.noarch
  fail2ban-server-1.0.1-2.el9.noarch                 libesmtp-1.0.6-24.el9.x86_64
  liblockfile-1.14-9.el9.x86_64                      python3-systemd-234-18.el9.x86_64

Complete!
```

- Lancement du service Fail2Ban

```bash
[scooby@proxy ~]$ sudo systemctl start fail2ban

[scooby@proxy ~]$ sudo systemctl enable fail2ban

Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.

[scooby@proxy ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-24 02:47:06 CET; 25s ago
       Docs: man:fail2ban(1)
   Main PID: 2606 (fail2ban-server)
      Tasks: 3 (limit: 5905)
     Memory: 10.6M
        CPU: 65ms
     CGroup: /system.slice/fail2ban.service
             └─2606 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Nov 24 02:47:06 proxy.tp3.linux systemd[1]: Starting Fail2Ban Service...
Nov 24 02:47:06 proxy.tp3.linux systemd[1]: Started Fail2Ban Service.
Nov 24 02:47:06 proxy.tp3.linux fail2ban-server[2606]: 2022-11-24 02:47:06,620 fail2ban.configreader >
Nov 24 02:47:06 proxy.tp3.linux fail2ban-server[2606]: Server ready
```

- Création des "Jails"

```bash
[scooby@proxy ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

[scooby@proxy ~]$ sudo vim /etc/fail2ban/jail.local

[scooby@proxy ~]$ sudo cat /etc/fail2ban/jail.local | grep ignore
[...]
ignoreip = 127.0.0.1/8 ::1
[...]
```

- On autorise Fail2Ban de travaillé avec Firewalld

```bash
[scooby@proxy ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local

[scooby@proxy ~]$ sudo systemctl restart fail2ban
```

- Création et modification d'un Jail pour ssh 

```bash
[scooby@proxy ~]$ sudo vim /etc/fail2ban/jail.d/sshd.local

[scooby@proxy ~]$ sudo cat /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true

# Override default values

bantime = -1
findtime = 1m
maxretry = 3

[scooby@proxy ~]$ sudo systemctl restart fail2ban*
```

- Vérification de la création du jail

```bash
[scooby@proxy ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```

- Verification du fonctionnement du fail2ban

```
PS C:\Users\glaro> ssh scooby@10.102.1.13
scooby@10.102.1.13's password:
Permission denied, please try again.

scooby@10.102.1.13's password:
Permission denied, please try again.

scooby@10.102.1.13's password:
scooby@10.102.1.13: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
```

- Verification du ban dans firewalld 

```bash
[scooby@proxy ~]$ sudo firewall-cmd --list-rich-rules
rule family="ipv4" source address="10.102.1.1" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
```

- Unban de l'utilisateur banni

```bash
[scooby@proxy ~]$ sudo fail2ban-client unban 10.102.1.1
1
```

- Verification que l'utilisateur n'est plus banni dans le firewalld

```bash
[scooby@proxy ~]$ sudo firewall-cmd --list-rich-rules

[scooby@proxy ~]$
```

- On se reconnecte a la vm à l'aide de ssh

```
PS C:\Users\glaro> ssh scooby@10.102.1.13
scooby@10.102.1.13's password:
Last login: Thu Nov 24 09:42:36 2022

[scooby@proxy ~]$
```