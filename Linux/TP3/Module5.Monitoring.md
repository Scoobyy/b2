# Module 5 : Monitoring

## Installation : 

- Installation sur **web.tp2.linux**
```bash
[scooby@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
-bash: wget: command not found

[scooby@web ~]$ sudo dnf install wget
Last metadata expiration check: 2:48:38 ago on Wed Nov 23 13:33:04 2022.
Dependencies resolved.
[...]
Installed:
  wget-1.21.1-7.el9.x86_64

Complete!

[scooby@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
--2022-11-23 16:26:11--  https://my-netdata.io/kickstart.sh
Resolving my-netdata.io (my-netdata.io)... 104.21.13.159, 172.67.156.192, 2606:4700:3031::6815:d9f, ...
[...]
Wed Nov 23 16:27:37 CET 2022 : INFO: netdata-updater.sh:  Auto-updating has been ENABLED through cron, updater script linked to /etc/cron.daily/netdata-updater\n
Wed Nov 23 16:27:37 CET 2022 : INFO: netdata-updater.sh:  If the update process fails and you have email notifications set up correctly for cron on this system, you should receive an email notification of the failure.
Wed Nov 23 16:27:37 CET 2022 : INFO: netdata-updater.sh:  Successful updates will not send an email.
Successfully installed the Netdata Agent.

Official documentation can be found online at https://learn.netdata.cloud/docs/.

Looking to monitor all of your infrastructure with Netdata? Check out Netdata Cloud at https://app.netdata.cloud.

Join our community and connect with us on:
  - GitHub: https://github.com/netdata/netdata/discussions
  - Discord: https://discord.gg/5ygS846fR6
  - Our community forums: https://community.netdata.cloud/
```

- Lancement du Service NetData

```bash
[scooby@web ~]$ sudo systemctl start netdata

[scooby@web ~]$ sudo systemctl enable netdata

[scooby@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
     Active: active (running) since Wed 2022-11-23 16:27:36 CET; 25min ago
   Main PID: 2546 (netdata)
      Tasks: 60 (limit: 5905)
     Memory: 104.6M
        CPU: 26.818s
     CGroup: /system.slice/netdata.service
             ├─2546 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
             ├─2548 /usr/sbin/netdata --special-spawn-server
             ├─2722 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
             ├─2728 /usr/libexec/netdata/plugins.d/apps.plugin 1
             └─2732 /usr/libexec/netdata/plugins.d/go.d.plugin 1
[...]
```

- Configuration du Firewall

```bash
# Recherche du port d'écoute de netdata
[scooby@web ~]$ sudo ss -laptn | grep netdata
LISTEN    0      4096            127.0.0.1:8125             0.0.0.0:*     users:(("netdata",pid=2546,fd=67))
LISTEN    0      4096              0.0.0.0:19999            0.0.0.0:*     users:(("netdata",pid=2546,fd=6))
LISTEN    0      4096                [::1]:8125                [::]:*     users:(("netdata",pid=2546,fd=66))
LISTEN    0      4096                 [::]:19999               [::]:*     users:(("netdata",pid=2546,fd=7))

[scooby@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success

[scooby@web ~]$ sudo firewall-cmd --reload
success

[scooby@web ~]$ sudo firewall-cmd --list-port
22/tcp 80/tcp 19999/tcp
```

## **On effectue les mêmes opérations sur db.tp2.linux**

### Configuration de Netdata pour l'envoie d'alert dans un channel textuel Discord 

- Création du fichier de configurations des alertes Netdata 

```bash
[scooby@web ~]$ sudo touch /etc/netdata/health.d/cpu_usage.conf
```

- Modification du fichier cpu_usage.conf

```bash
[scooby@web ~]$ cd /etc/netdata/

[scooby@web netdata]$ sudo ./edit-config health.d/cpu_usage.conf
Editing '/etc/netdata/health.d/cpu_usage.conf' ...

[scooby@web netdata]$ cat health.d/cpu_usage.conf
alarm: cpu_usage
on: system.cpu
lookup: average -3s percentage foreach user,system
units: %
every: 10s
warn: $this > 50
crit: $this > 80
info: CPU utilization of users or the system itself
```

- Lancement des alertes

```bash
[scooby@web netdata]$ sudo netdatacli reload-health
```

- Ajout du webhook dans la config des alertes

```bash
[scooby@web ~]$ sudo cat /etc/netdata//health_alarm_notify.conf | grep DISCORD | head -n 3
SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1045074864119230545/EirzafLiAFyjOwF_V-H6RZvgtq2lY9ME8U7ksVfzCsS-kazBxbsvpa9Gr1s_FHJ9bbrV"
DEFAULT_RECIPIENT_DISCORD="alarm-cpu"
```

### Verification des alertes avec un test de stress sur la RAM

- Installation de stress

```bash
[scooby@web ~]$ sudo dnf install stress-ng -y
Last metadata expiration check: 0:19:28 ago on Wed Nov 23 23:59:50 2022.
Dependencies resolved.
[...]
Installed:
  Judy-1.0.5-28.el9.x86_64     lksctp-tools-1.0.19-1.el9.x86_64     stress-ng-0.13.10-1.el9.x86_64

Complete!
```

- Vérification des alertes

```bash
[scooby@web ~]$ sudo stress-ng -c 10 -l 60
stress-ng: info:  [6861] defaulting to a 86400 second (1 day, 0.00 secs) run per stressor
stress-ng: info:  [6861] dispatching hogs: 10 cpu
^Cstress-ng: info:  [6861] successful run completed in 20.92s
```

Voici le lien du [message d'alerte](https://discord.com/channels/1045070295150641223/1045070295683313706/1045117351554719775) 
