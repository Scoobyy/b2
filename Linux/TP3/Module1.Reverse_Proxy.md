# Module 1 : Reverse Proxy

## Sommaire

- [Module 1 : Reverse Proxy](#module-1--reverse-proxy)
    - [Sommaire](#sommaire)
    - [I. Setup](#i-setup)
    - [III. HTTPS](#iii-https)

# I. Setup

🖥️ **VM `proxy.tp3.linux`**

**N'oubliez pas de dérouler la [📝**checklist**📝](../../2/README.md#checklist).**

➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`
```bash
[scooby@proxy ~]$ sudo dnf install nginx
[sudo] password for scooby:
Last metadata expiration check: 0:30:05 ago on Tue Nov 22 15:39:33 2022.
Dependencies resolved.
[...]
Installed:
  nginx-1:1.20.1-10.el9.x86_64     nginx-filesystem-1:1.20.1-10.el9.noarch     rocky-logos-httpd-90.11-1.el9.noarch

Complete!
```
- démarrer le service `nginx`
```bash
[scooby@proxy ~]$ sudo systemctl start nginx

[scooby@proxy ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
```
- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute
```bash
[scooby@proxy ~]$ sudo ss -laptn | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*     users:(("nginx",pid=1030,fd=6),("nginx",pid=1029,fd=6))
LISTEN 0      511             [::]:80           [::]:*     users:(("nginx",pid=1030,fd=7),("nginx",pid=1029,fd=7))
```

- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX
```bash
[scooby@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[scooby@proxy ~]$ sudo firewall-cmd --list-port
80/tcp
```
- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX
```bash
[scooby@proxy ~]$ ps -ef | grep nginx
root        1029       1  0 16:14 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1030    1029  0 16:14 ?        00:00:00 nginx: worker process
scooby      1087     831  0 16:35 pts/0    00:00:00 grep --color=auto nginx
```

- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine
```bash
[scooby@proxy ~]$ curl 10.102.1.13:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
    [...]
    <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

➜ **Configurer NGINX**

- nous ce qu'on veut, c'pas une page d'accueil moche, c'est que NGINX agisse comme un reverse proxy entre les clients et notre serveur Web
- deux choses à faire :
  - créer un fichier de configuration NGINX
    - la conf est dans `/etc/nginx`
    - procédez comme pour Apache : repérez les fichiers inclus par le fichier de conf principal, et créez votre fichier de conf en conséquence
    ```bash
    # Verification de l'emplacement des fichiers de conf
    [scooby@proxy ~]$ sudo tail -n 14 /etc/nginx/nginx.conf | head -n 2
    #        # Load configuration files for the default server block.
    #        include /etc/nginx/default.d/*.conf;
    
    [scooby@proxy ~]$ cd /etc/nginx/

    [scooby@proxy nginx]$ ls
    conf.d        fastcgi.conf.default    koi-utf     mime.types.default  scgi_params          uwsgi_params.default
    default.d     fastcgi_params          koi-win     nginx.conf          scgi_params.default  win-utf
    fastcgi.conf  fastcgi_params.default  mime.types  nginx.conf.default  uwsgi_params

    [scooby@proxy nginx]$ ls default.d/

    # Creation du fichier conf pour le reverse Proxy en changeant <IP_DE_NEXTCLOUD> par l'adresse ip de web : 10.102.1.11
    [scooby@proxy nginx]$ sudo vim default.d/proxy.conf
    ```
  - NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
    - y'a donc un fichier de conf NextCloud à modifier
    - c'est un fichier appelé `config.php`

    ```bash
    [scooby@web ~]$ sudo vim /var/www/tp2_nextcloud/config/config.php

    [scooby@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
    <?php
    $CONFIG = array (
      'instanceid' => 'ocamyqbgeh01',
      'passwordsalt' => 'G6lFir54UV1aSCcrEw2jXphpg0ccG6',
      'secret' => 'cs9WkDCb1D12suNN4xOHHAiGglqbckGEXIy0kLmZ6QdKq85X',
      'trusted_domains' =>
      array (
              0 => 'web.tp2.linux',
              1 => '10.102.1.13',
      ),
      'datadirectory' => '/var/www/tp2_nextcloud/data',
      'dbtype' => 'mysql',
      'version' => '25.0.0.15',
      'overwrite.cli.url' => 'http://web.tp2.linux',
      'dbname' => 'nextcloud',
      'dbhost' => '10.102.1.12:3306',
      'dbport' => '',
      'dbtableprefix' => 'oc_',
      'mysql.utf8mb4' => true,
      'dbuser' => 'nextcloud',
      'dbpassword' => 'pewpewpew',
      'installed' => true,
      'maintenance' => false
    );
    ```

Référez-vous à monsieur Google pour tout ça :)

Exemple de fichier de configuration minimal NGINX.:

```nginx
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://<IP_DE_NEXTCLOUD>:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

➜ **Modifier votre fichier `hosts` de VOTRE PC**

- pour que le service soit joignable avec le nom `web.tp2.linux`
- c'est à dire que `web.tp2.linux` doit pointer vers l'IP de `proxy.tp3.linux`
- autrement dit, pour votre PC :
  - `web.tp2.linux` pointe vers l'IP du reverse proxy
  - `proxy.tp3.linux` ne pointe vers rien
  ```bash
  # Dans le fichier Hosts (C:\Windows\System32\drivers\etc\)
  
  # Copyright (c) 1993-2009 Microsoft Corp.
  #
  # This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
  #
  # This file contains the mappings of IP addresses to host names. Each
  # entry should be kept on an individual line. The IP address should
  # be placed in the first column followed by the corresponding host name.
  # The IP address and the host name should be separated by at least one
  # space.
  #
  # Additionally, comments (such as these) may be inserted on individual
  # lines or following the machine name denoted by a '#' symbol.
  #
  # For example:
  #
  #      102.54.94.97     rhino.acme.com          # source server
  #       38.25.63.10     x.acme.com              # x client host

  # localhost name resolution is handled within DNS itself.
  #	127.0.0.1       localhost
  #	::1             localhost

  10.102.1.13 web.tp2.linux
  ```
  - taper `http://web.tp2.linux` permet d'accéder au site (en passant de façon transparente par l'IP du proxy)

> Oui vous ne rêvez pas : le nom d'une machine donnée pointe vers l'IP d'une autre ! Ici, on fait juste en sorte qu'un certain nom permette d'accéder au service, sans se soucier de qui porte réellement ce nom.

# III. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

- Installation des outils pour le https
```bash
[scooby@proxy ~]$ sudo dnf install epel-release
Rocky Linux 9 - BaseOS                                                5.7 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                             7.2 kB/s | 3.6 kB     00:00
Rocky Linux 9 - CRB                                                   7.4 kB/s | 3.6 kB     00:00
Rocky Linux 9 - Extras                                                5.9 kB/s | 2.9 kB     00:00
Package epel-release-9-4.el9.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!

[scooby@proxy ~]$ sudo dnf install certbot python3-certbot-nginx
Last metadata expiration check: 0:01:00 ago on Wed Nov 23 11:32:04 2022.
Dependencies resolved.
[...]
Installed:
  certbot-1.32.0-1.el9.noarch                     python-josepy-doc-1.13.0-1.el9.noarch
  python3-acme-1.32.0-1.el9.noarch                python3-certbot-1.32.0-1.el9.noarch
  python3-certbot-nginx-1.32.0-1.el9.noarch       python3-cffi-1.14.5-5.el9.x86_64
  python3-chardet-4.0.0-5.el9.noarch              python3-configargparse-1.5.3-1.el9.noarch
  python3-configobj-5.0.6-25.el9.noarch           python3-cryptography-36.0.1-1.el9_0.x86_64
  python3-distro-1.5.0-7.el9.noarch               python3-idna-2.10-7.el9.noarch
  python3-josepy-1.13.0-1.el9.noarch              python3-parsedatetime-2.6-5.el9.noarch
  python3-ply-3.11-14.el9.noarch                  python3-pyOpenSSL-21.0.0-1.el9.noarch
  python3-pycparser-2.20-6.el9.noarch             python3-pyparsing-2.4.7-9.el9.noarch
  python3-pyrfc3339-1.1-11.el9.noarch             python3-pysocks-1.7.1-12.el9.noarch
  python3-pytz-2021.1-4.el9.noarch                python3-requests-2.25.1-6.el9.noarch
  python3-requests-toolbelt-0.9.1-16.el9.noarch   python3-urllib3-1.26.5-3.el9.noarch
  python3-zope-component-4.3.0-19.el9.noarch      python3-zope-event-4.5.0-1.el9~bootstrap.1.noarch
  python3-zope-interface-5.4.0-5.el9.1.x86_64

Complete!
```

- Mise en place du firewall 
```bash
[scooby@proxy ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success

[scooby@proxy ~]$ sudo firewall-cmd --add-service=https --permanent
success

[scooby@proxy ~]$ sudo firewall-cmd --reload
success
```

- on génère une paire de clés sur le serveur `proxy.tp3.linux`
  ```bash
  [scooby@proxy ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
  [...]
  -----
  You are about to be asked to enter information that will be incorporated
  into your certificate request.
  What you are about to enter is what is called a Distinguished Name or a DN.
  There are quite a few fields but you can leave some blank
  For some fields there will be a default value,
  If you enter '.', the field will be left blank.
  -----
  Country Name (2 letter code) [XX]:
  State or Province Name (full name) []:
  Locality Name (eg, city) [Default City]:
  Organization Name (eg, company) [Default Company Ltd]:
  Organizational Unit Name (eg, section) []:
  Common Name (eg, your name or your server's hostname) []:web.tp2.linux
  Email Address []:

  [scooby@proxy ~]$ mv server.crt web.tp2.linux.crt

  [scooby@proxy ~]$ mv server.key web.tp2.linux.key

  [scooby@proxy ~]$ sudo mv web.tp2.linux.crt /etc/pki/tls/certs

  [scooby@proxy ~]$ sudo mv web.tp2.linux.key /etc/pki/tls/private
  ```
- on ajuste la conf NGINX
  - on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic
  - on lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP
  ```bash
  [scooby@proxy ~]$ sudo vim /etc/nginx/nginx.

  [scooby@proxy ~]$ sudo cat /etc/nginx/nginx.conf
  [...]
      server {
          listen       443 ssl;
          server_name  web.tp2.linux;
          ssl_certificate /etc/pki/tls/certs/web.tp2.linux.crt;
          ssl_certificate_key /etc/pki/tls/private/web.tp2.linux.key;

          location / {
                  # On définit des headers HTTP pour que le proxying se passe bien
                  proxy_set_header  Host $host;
                  proxy_set_header  X-Real-IP $remote_addr;
                  proxy_set_header  X-Forwarded-Proto https;
                  proxy_set_header  X-Forwarded-Host $remote_addr;
                  proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

                  proxy_pass http://10.102.1.11:80;
          }

          location /.well-known/carddav {
                  return 301 $scheme://$host/remote.php/dav;
          }

          location /.well-known/caldav {
                  return 301 $scheme://$host/remote.php/dav;
          }
      }
    [...]
  ```

- On informe Nextcloud que maintenant on est sur du https
```bash
[scooby@web ~]$ sudo vim /var/www/tp2_nextcloud/config/config.php
[scooby@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
...
  'overwrite.cli.url' => 'https://web.tp2.linux',
  'overwriteprotocol' => 'https',
...
[scooby@web ~]$ sudo systemctl restart httpd
```

