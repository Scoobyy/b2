# Module 2 : Replication de base de données

- Installation de MariaDB serveur

```bash
[scooby@rpl ~]$ sudo dnf install mariadb-server -y
Last metadata expiration check: 0:16:17 ago on Thu Nov 24 10:09:32 2022.
Dependencies resolved.
[...]
perl-libnet-3.13-4.el9.noarch                    perl-libs-4:5.32.1-479.el9.x86_64
  perl-mro-1.23-479.el9.x86_64                     perl-overload-1.31-479.el9.noarch
  perl-overloading-0.02-479.el9.noarch             perl-parent-1:0.238-460.el9.noarch
  perl-podlators-1:4.14-460.el9.noarch             perl-subs-1.03-479.el9.noarch
  perl-vars-1.05-479.el9.noarch                    policycoreutils-python-utils-3.3-6.el9_0.noarch
  python3-audit-3.0.7-101.el9_0.2.x86_64           python3-libsemanage-3.3-2.el9.x86_64
  python3-policycoreutils-3.3-6.el9_0.noarch       python3-setools-4.4.0-4.el9.x86_64
  python3-setuptools-53.0.0-10.el9.noarch

Complete!
```

- Lancement du service MariaDB

```bash
[scooby@rpl ~]$ sudo systemctl start mariadb

[scooby@rpl ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

- Installation secure de MariaDB

```bash
[scooby@rpl ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!
[...]
Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

- Préparation du Master Node

```sql
MariaDB [(none)]> SET sql_log_bin=1;
Query OK, 0 rows affected (0.000 sec)
```