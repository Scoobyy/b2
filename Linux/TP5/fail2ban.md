# **Fail2ban**

- Les commandes s'effecturons sur ces deux vm

| Machine                       | IP            | Service                   |
|-------------------------------|---------------|---------------------------|
| `reverseproxy.mattermost.srv` | `10.107.1.13` | Reverse Proxy             |
| `db.mattermost.srv`           | `10.107.1.12` | Base de donnée (mariaDB)  |

- Installation de Fail2Ban

```bash
[scooby@reverseproxy ~]$ sudo dnf install fail2ban fail2ban-firewalld -y
[sudo] password for scooby:
Extra Packages for Enterprise Linux 9 - x86_64                                          9.2 kB/s |  28 kB     00:03
Extra Packages for Enterprise Linux 9 - x86_64                                          2.8 MB/s |  12 MB     00:04
Rocky Linux 9 - BaseOS                                                                  4.5 kB/s | 3.6 kB     00:00
Rocky Linux 9 - BaseOS                                                                  2.1 MB/s | 1.7 MB     00:00
Rocky Linux 9 - AppStream                                                                12 kB/s | 4.1 kB     00:00
Rocky Linux 9 - Extras                                                                  8.1 kB/s | 2.9 kB     00:00
Dependencies resolved.
[...]
Installed:
  esmtp-1.2-19.el9.x86_64                 fail2ban-1.0.1-2.el9.noarch           fail2ban-firewalld-1.0.1-2.el9.noarch
  fail2ban-sendmail-1.0.1-2.el9.noarch    fail2ban-server-1.0.1-2.el9.noarch    libesmtp-1.0.6-24.el9.x86_64
  liblockfile-1.14-10.el9.x86_64          python3-systemd-234-18.el9.x86_64

Complete!
```

- Lancement du service Fail2ban

```bash
[scooby@reverseproxy ~]$ sudo systemctl start fail2ban


[scooby@reverseproxy ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.

[scooby@reverseproxy ~]$ systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Sun 2022-12-11 16:19:16 CET; 10min ago
       Docs: man:fail2ban(1)
   Main PID: 1161 (fail2ban-server)
      Tasks: 3 (limit: 5905)
     Memory: 13.1M
        CPU: 91ms
     CGroup: /system.slice/fail2ban.service
             └─1161 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Dec 11 16:19:16 reverseproxy.mattermost.srv systemd[1]: Starting Fail2Ban Service...
Dec 11 16:19:16 reverseproxy.mattermost.srv systemd[1]: Started Fail2Ban Service.
Dec 11 16:19:16 reverseproxy.mattermost.srv fail2ban-server[1161]: 2022-12-11 16:19:16,164 fail2ban.configreader   [116>
Dec 11 16:19:16 reverseproxy.mattermost.srv fail2ban-server[1161]: Server ready
```

- Creation des Jails

```bash
[scooby@reverseproxy ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

[scooby@reverseproxy ~]$ sudo vim /etc/fail2ban/jail.local

[scooby@reverseproxy ~]$ sudo cat /etc/fail2ban/jail.local | grep ignore
[...]
ignoreip = 127.0.0.1/8 ::1
[...]
```

- Autorisation de Fail2ban d'utiliser Firewalld

```bash
[scooby@reverseproxy ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local

[scooby@reverseproxy ~]$ sudo systemctl restart fail2ban
```

- Création et modification d'un Jail pour SSH

```bash
[scooby@reverseproxy ~]$ sudo vim /etc/fail2ban/jail.d/sshd.local

[scooby@reverseproxy ~]$ sudo cat /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true

# Override default values

bantime = -1
findtime = 1m
maxretry = 3

[scooby@reverseproxy ~]$ sudo systemctl restart fail2ban
```

- Vérification que la Jail a bien été créé

```bash
[scooby@reverseproxy ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```

- Vérification du fonctionnement de Fail2ban

```bash
PS C:\Users\glaro> ssh scooby@10.107.1.13
scooby@10.107.1.13's password:
Permission denied, please try again.

scooby@10.107.1.13's password:
Permission denied, please try again.

scooby@10.107.1.13's password:
scooby@10.107.1.13: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
```

- Vérification du ban dans le firewall

```bash
[scooby@reverseproxy ~]$ sudo firewall-cmd --list-rich-rules
rule family="ipv4" source address="10.107.1.1" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
```

- Unban de l'utilisateur banni

```bash
[scooby@reverseproxy ~]$ sudo fail2ban-client unban 10.107.1.1
1
```

- Vérification que l'utilisateur est bien débanni

```bash
[scooby@reverseproxy ~]$ sudo firewall-cmd --list-rich-rules
```

- On se reconnecte pour être sûr

```bash
PS C:\Users\glaro> ssh scooby@10.107.1.13
scooby@10.107.1.13's password:
Last login: Sun Dec 11 15:38:34 2022 from 10.107.1.1
[scooby@reverseproxy ~]$
```