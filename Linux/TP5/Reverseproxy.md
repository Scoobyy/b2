# **Reverse Proxy et HTTPS**

| Machine                      | IP            | Service              |
|------------------------------|---------------|----------------------|
| `reverseproxy.mattermost.srv`| `10.107.1.13` | Reverse Proxy (nginx)|

- Installation Reverse Proxy

```bash
[scooby@reverseproxy ~]$ sudo dnf install nginx -y
[sudo] password for scooby:
Last metadata expiration check: 0:46:33 ago on Mon Dec  5 14:57:24 2022.
Dependencies resolved.
[...]
Installed:
  nginx-1:1.20.1-13.el9.x86_64                        nginx-core-1:1.20.1-13.el9.x86_64
  nginx-filesystem-1:1.20.1-13.el9.noarch             rocky-logos-httpd-90.13-1.el9.noarch

Complete!
```

- Demarrage du service `nginx`
```bash
[scooby@reverseproxy ~]$ sudo systemctl start nginx

[scooby@reverseproxy ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
```

- Ouverture d'un port dans le firewall pour autoriser le trafic de `nginx`
```bash
#On cherche le port sur lequel NGINX écoute
[scooby@reverseproxy ~]$ sudo ss -laptn | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*     users:(("nginx",pid=1120,fd=6),("nginx",pid=1119,fd=6))
LISTEN 0      511             [::]:80           [::]:*     users:(("nginx",pid=1120,fd=7),("nginx",pid=1119,fd=7))

# Ouverture du port 80 dabs le firewall
[scooby@reverseproxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success

[scooby@reverseproxy ~]$ sudo firewall-cmd --reload
success

[scooby@reverseproxy ~]$ sudo firewall-cmd --list-port
80/tcp
```

- Verification de la page d'accueil NGINX sur l'ip de la vm et le port 80
```bash
[scooby@reverseproxy ~]$ curl 10.107.1.13:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
    [...]
    <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

### **Configuration NGINX**

- Création d'un fichier de conf NGINX

```bash
# Verification de l'emplacement des fichiers de conf
[scooby@proxy ~]$ sudo tail -n 14 /etc/nginx/nginx.conf | head -n 2
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;

[scooby@proxy nginx]$ sudo vim /etc/nginx/default.d/proxy.conf

[scooby@reverseproxy nginx]$ cat /etc/nginx/conf.d/reverseproxy.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name chat.mattermost.srv;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.107.1.11:8065;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

## **HTTPS**

- Installation des outils nécessaire pour le https
```bash
[scooby@reverseproxy ~]$ sudo dnf install epel-release -y
Last metadata expiration check: 1:41:17 ago on Mon Dec  5 14:57:24 2022.
Dependencies resolved.
[...]
Installed:
  epel-release-9-4.el9.noarch

Complete!

[scooby@reverseproxy ~]$ sudo dnf install certbot python3-certbot-nginx -y
Extra Packages for Enterprise Linux 9 - x86_64                   2.3 MB/s |  12 MB     00:05
Last metadata expiration check: 0:00:05 ago on Mon Dec  5 16:39:41 2022.
Dependencies resolved.
[...]
Installed:
  certbot-1.32.0-1.el9.noarch                        python-josepy-doc-1.13.0-1.el9.noarch
  python3-acme-1.32.0-1.el9.noarch                   python3-certbot-1.32.0-1.el9.noarch
  python3-certbot-nginx-1.32.0-1.el9.noarch          python3-cffi-1.14.5-5.el9.x86_64
  python3-chardet-4.0.0-5.el9.noarch                 python3-configargparse-1.5.3-1.el9.noarch
  python3-configobj-5.0.6-25.el9.noarch              python3-cryptography-36.0.1-2.el9.x86_64
  python3-distro-1.5.0-7.el9.noarch                  python3-idna-2.10-7.el9.noarch
  python3-josepy-1.13.0-1.el9.noarch                 python3-parsedatetime-2.6-5.el9.noarch
  python3-ply-3.11-14.el9.noarch                     python3-pyOpenSSL-21.0.0-1.el9.noarch
  python3-pycparser-2.20-6.el9.noarch                python3-pyparsing-2.4.7-9.el9.noarch
  python3-pyrfc3339-1.1-11.el9.noarch                python3-pysocks-1.7.1-12.el9.noarch
  python3-pytz-2021.1-4.el9.noarch                   python3-requests-2.25.1-6.el9.noarch
  python3-requests-toolbelt-0.9.1-16.el9.noarch      python3-setuptools-53.0.0-10.el9.noarch
  python3-urllib3-1.26.5-3.el9.noarch                python3-zope-component-4.3.0-19.el9.noarch
  python3-zope-event-4.5.0-1.el9~bootstrap.1.noarch  python3-zope-interface-5.4.0-5.el9.1.x86_64

Complete!
```

- Mise en place du firewall
```bash
[scooby@reverseproxy ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success

[scooby@reverseproxy ~]$ sudo firewall-cmd --add-service=https --permanent
success

[scooby@reverseproxy ~]$ sudo firewall-cmd --reload
success

[scooby@reverseproxy ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client https ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

- on génère une paire de clés sur le serveur `reverseproxy.mattermost.srv`
```bash
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server s hostname) []:chat.mattermost.srv
Email Address []:

[scooby@reverseproxy ~]$ mv server.crt chat.mattermost.srv.crt

[scooby@reverseproxy ~]$ mv server.key chat.mattermost.srv.key

[scooby@reverseproxy ~]$ sudo mv chat.mattermost.srv.crt /etc/pki/tls/certs

[scooby@reverseproxy ~]$ sudo mv chat.mattermost.srv.key /etc/pki/tls/private
```

- Ajustement de la conf NGINX pour le https
```bash
[scooby@reverseproxy ~]$ sudo vim /etc/nginx/conf.d/reverseproxy.conf

[scooby@reverseproxy ~]$ sudo cat /etc/nginx/conf.d/reverseproxy.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name chat.mattermost.srv;

    # Port d'écoute de NGINX
    listen 443 ssl;

    ssl_certificate /etc/pki/tls/certs/chat.mattermost.srv.crt;
    ssl_certificate_key /etc/pki/tls/private/chat.mattermost.srv.key;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.107.1.11:8065;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```