# **Sauvegarde Base de donnée**

| Machine                       | IP            | Service                   |
|-------------------------------|---------------|---------------------------|
| `restore.masttermost.srv`     | `10.107.1.14` | Save Base de donnée       |

## Mise en place d'un serveur NFS

## Installe serveur NFS
- Création du fichier qui va être pratagé

```bash
[scooby@restore ~]$ sudo mkdir -p /srv/nfs_shares/shared
```

- Install du package nfs

```bash
[scooby@restore ~]$ sudo dnf install nfs-utils
Last metadata expiration check: 0:36:19 ago on Sun Dec 11 21:36:46 2022.
Dependencies resolved.
[...]
Installed:
  gssproxy-0.8.4-4.el9.x86_64
  keyutils-1.6.1-4.el9.x86_64
  libev-4.33-5.el9.x86_64
  libnfsidmap-1:2.5.4-15.el9.x86_64
  libtirpc-1.3.3-0.el9.x86_64
  libverto-libev-0.3.2-3.el9.x86_64
  nfs-utils-1:2.5.4-15.el9.x86_64
  python3-pyyaml-5.4.1-6.el9.x86_64
  quota-1:4.06-6.el9.x86_64
  quota-nls-1:4.06-6.el9.noarch
  rpcbind-1.2.6-5.el9.x86_64
  sssd-nfs-idmap-2.7.3-4.el9_1.1.x86_64

Complete!
```
- Mise en place de l'utilisateur `backup`
```bash
[scooby@restore ~]$ sudo useradd backup -m -d /srv/nfs_shares/shared/ -
s /usr/bin/nologin
useradd: Warning: missing or non-executable shell '/usr/bin/nologin'
useradd: warning: the home directory /srv/nfs_shares/shared/ already exists.
useradd: Not copying any file from skel directory into it.

[scooby@restore ~]$ sudo chown backup:backup /srv/nfs_shares/shared/
```

- Autorisation du fichier a être partagé par NFS

```bash
[scooby@restore ~]$ sudo vim /etc/exports

[scooby@restore ~]$ sudo cat /etc/exports
/srv/nfs_shares/shared 10.107.1.12(rw,sync,no_subtree_check)
```

- Lancement du service NFS 

```bash
[scooby@restore ~]$ sudo systemctl start nfs-server

[scooby@restore ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

[scooby@restore ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
     Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabl>
    Drop-In: /run/systemd/generator/nfs-server.service.d
             └─order-with-mounts.conf
     Active: active (exited) since Sun 2022-12-11 22:41:47 CET; 33s ago
   Main PID: 3092 (code=exited, status=0/SUCCESS)
        CPU: 12ms

Dec 11 22:41:47 restore.masttermost.srv systemd[1]: Starting NFS serve>
Dec 11 22:41:47 restore.masttermost.srv systemd[1]: Finished NFS serve>
```

- Ajouts des services au firewall nécessaire au serveur NFS

```bash
[scooby@restore ~]$ sudo firewall-cmd --add-service=nfs --permanent
success

[scooby@restore ~]$ sudo firewall-cmd --add-service=mountd --permanent
success

[scooby@restore ~]$ sudo firewall-cmd --add-service=rpc-bind --permanent
success

[scooby@restore ~]$ sudo firewall-cmd --reload
success

[scooby@restore ~]$ sudo firewall-cmd --list-services
cockpit dhcpv6-client mountd nfs rpc-bind ssh
```

## Installe client NFS

- Installation de nfs

```bash
[scooby@db ~]$ sudo dnf install nfs-utils -y
Last metadata expiration check: 1:31:30 ago on Sun Dec 11 21:25:18 2022.
Dependencies resolved.
[...]
Installed:
  gssproxy-0.8.4-4.el9.x86_64
  keyutils-1.6.1-4.el9.x86_64
  libev-4.33-5.el9.x86_64
  libnfsidmap-1:2.5.4-15.el9.x86_64
  libtirpc-1.3.3-0.el9.x86_64
  libverto-libev-0.3.2-3.el9.x86_64
  nfs-utils-1:2.5.4-15.el9.x86_64
  python3-pyyaml-5.4.1-6.el9.x86_64
  quota-1:4.06-6.el9.x86_64
  quota-nls-1:4.06-6.el9.noarch
  rpcbind-1.2.6-5.el9.x86_64
  sssd-nfs-idmap-2.7.3-4.el9_1.1.x86_64

Complete!
```

- Création du lien entre le serveur et le client

```bash
[scooby@db ~]$ sudo mkdir /srv/backup/

[scooby@db ~]$ sudo mount 10.107.1.14:/srv/nfs_shares/shared /srv/backup/
```

- Création du user client

```bash
[scooby@db ~]$ sudo useradd backup -m -d /srv/backup -s /usr/bin/nologi
n
useradd: Warning: missing or non-executable shell '/usr/bin/nologin'
useradd: warning: the home directory /srv/backup already exists.
useradd: Not copying any file from skel directory into it.

[scooby@db ~]$ ls -l /srv/
total 0
drwxr-xr-x. 2 backup backup 6 Dec 11 22:11 backup

[scooby@db ~]$ sudo -u backup touch /srv/backup/test.txt
```

- Vérification que le fichier est bien partagé

```bash
[scooby@restore ~]$ ls -l /srv/nfs_shares/shared/
total 0
-rw-r--r--. 1 backup backup 0 Dec 11 23:06 test.txt
```

- Maintenant on automatise que le dossier se mount automatique au démarrage du serveur

```bash
[scooby@db ~]$ sudo vim /etc/fstab
[scooby@db ~]$ sudo cat /etc/fstab
[...]
10.107.1.14:/srv/nfs_shares/shared /srv/backup nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
```

- Verification de l'automatisation

```bash
[scooby@db ~]$ sudo reboot

[scooby@db ~]$ sudo -u backup rm /srv/backup/test.txt

# On regarde maintenant sur l'autre machine si le fichier a bien été supprimé
[scooby@restore ~]$ ls -l /srv/nfs_shares/shared/
total 0
```

- Installation de MariaDB 

```bash
[scooby@restore ~]$ sudo dnf install mariadb -y
Last metadata expiration check: 0:15:49 ago on Sun Dec 11 21:36:46 2022.
Dependencies resolved.
[...]
Installed:
  mariadb-3:10.5.16-2.el9_0.x86_64
  mariadb-common-3:10.5.16-2.el9_0.x86_64
  mariadb-connector-c-3.2.6-1.el9_0.x86_64
  mariadb-connector-c-config-3.2.6-1.el9_0.noarch
  perl-AutoLoader-5.74-479.el9.noarch
  perl-B-1.80-479.el9.x86_64
  perl-Carp-1.50-460.el9.noarch
  perl-Class-Struct-0.66-479.el9.noarch
  perl-Data-Dumper-2.174-462.el9.x86_64
  perl-Digest-1.19-4.el9.noarch
  perl-Digest-MD5-2.58-4.el9.x86_64
  perl-Encode-4:3.08-462.el9.x86_64
  perl-Errno-1.30-479.el9.x86_64
  perl-Exporter-5.74-461.el9.noarch
  perl-Fcntl-1.13-479.el9.x86_64
  perl-File-Basename-2.85-479.el9.noarch
  perl-File-Path-2.18-4.el9.noarch
  perl-File-Temp-1:0.231.100-4.el9.noarch
  perl-File-stat-1.09-479.el9.noarch
  perl-FileHandle-2.03-479.el9.noarch
  perl-Getopt-Long-1:2.52-4.el9.noarch
  perl-Getopt-Std-1.12-479.el9.noarch
  perl-HTTP-Tiny-0.076-460.el9.noarch
  perl-IO-1.43-479.el9.x86_64
  perl-IO-Socket-IP-0.41-5.el9.noarch
  perl-IO-Socket-SSL-2.073-1.el9.noarch
  perl-IPC-Open3-1.21-479.el9.noarch
  perl-MIME-Base64-3.16-4.el9.x86_64
  perl-Mozilla-CA-20200520-6.el9.noarch
  perl-NDBM_File-1.15-479.el9.x86_64
  perl-Net-SSLeay-1.92-2.el9.x86_64
  perl-POSIX-1.94-479.el9.x86_64
  perl-PathTools-3.78-461.el9.x86_64
  perl-Pod-Escapes-1:1.07-460.el9.noarch
  perl-Pod-Perldoc-3.28.01-461.el9.noarch
  perl-Pod-Simple-1:3.42-4.el9.noarch
  perl-Pod-Usage-4:2.01-4.el9.noarch
  perl-Scalar-List-Utils-4:1.56-461.el9.x86_64
  perl-SelectSaver-1.02-479.el9.noarch
  perl-Socket-4:2.031-4.el9.x86_64
  perl-Storable-1:3.21-460.el9.x86_64
  perl-Symbol-1.08-479.el9.noarch
  perl-Sys-Hostname-1.23-479.el9.x86_64
  perl-Term-ANSIColor-5.01-461.el9.noarch
  perl-Term-Cap-1.17-460.el9.noarch
  perl-Text-ParseWords-3.30-460.el9.noarch
  perl-Text-Tabs+Wrap-2013.0523-460.el9.noarch
  perl-Time-Local-2:1.300-7.el9.noarch
  perl-URI-5.09-3.el9.noarch
  perl-base-2.27-479.el9.noarch
  perl-constant-1.33-461.el9.noarch
  perl-if-0.60.800-479.el9.noarch
  perl-interpreter-4:5.32.1-479.el9.x86_64
  perl-libnet-3.13-4.el9.noarch
  perl-libs-4:5.32.1-479.el9.x86_64
  perl-mro-1.23-479.el9.x86_64
  perl-overload-1.31-479.el9.noarch
  perl-overloading-0.02-479.el9.noarch
  perl-parent-1:0.238-460.el9.noarch
  perl-podlators-1:4.14-460.el9.noarch
  perl-subs-1.03-479.el9.noarch
  perl-vars-1.05-479.el9.noarch

Complete!
```

- Création d'un user pour la backup

```sql
MariaDB [(none)]> CREATE USER 'backup'@'10.107.1.14' IDENTIFIED BY 'toto';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON mattermost.* TO 'backup'@'10.107.1.14';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

- Installation du package `tar`

```bash
[scooby@db ~]$ sudo dnf install tar -y
[sudo] password for scooby:
Last metadata expiration check: 2:04:36 ago on Sun Dec 11 21:25:18 2022.
Dependencies resolved.
[...]
Installed:
  tar-2:1.34-5.el9.x86_64

Complete!
```

- Création du Script `save.sh`

```bash
[scooby@db ~]$ cd /srv/

[scooby@db srv]$ sudo vim save.sh

[scooby@db srv]$ sudo cat save.sh
#!/bin/bash
# Date : 11/12/22
# Written by : Laroumanie Gabriel
# This script will dump the database and save it to a file

# Set the variables
user='backup'
passwd='toto'
db='mattermost'
ip_serv='localhost'
datesauv=$(date '+%y%m%d_%H%M%S')
name="${db}_${datesauv}"
outputpath="/srv/backup/"

# Dump the database

echo "Backup started for database - ${db}."

cd $outputpath

mysqldump -h ${ip_serv} -u ${user} -p${passwd} --skip-lock-tables --databases ${db} > "${name}.sql"
if [[ $? == 0 ]]
then
        tar -czf "${name}.tar.gz" "${name}.sql"
        rm -f "${name}.sql"
        echo "Backup successfully completed."
else
        echo "Backup failed."
        rm -f "${name}.sql"
        exit 1
fi
```

- Mise en place du script

```bash
[scooby@db srv]$ sudo chmod 744 save.sh

[scooby@db srv]$ ls -l
total 4
drwxr-xr-x. 2 backup backup   6 Dec 11 23:15 backup
-rwxr--r--. 1 root   root   672 Dec 11 23:37 save.sh

[scooby@db srv]$ sudo chown backup:backup save.sh

[scooby@db srv]$ ls -l /srv/
total 4
drwxr-xr-x. 2 backup backup   6 Dec 11 23:15 backup
-rwxr--r--. 1 backup backup 690 Dec 11 23:42 save.sh
```

- Test du script

```bash
[scooby@db srv]$ sudo -u backup /srv/save.sh
Backup started for database - mattermost.
Backup successfully completed.

[scooby@db srv]$ ls -l backup/
total 104
-rw-r--r--. 1 backup backup 106184 Dec 12 00:08 mattermost_221212_000808.tar.gz

[scooby@db srv]$ cd backup/

[scooby@db backup]$ sudo -u backup tar -xzf mattermost_221212_000808.ta
r.gz

[scooby@db backup]$ ls -l
total 628
-rw-r--r--. 1 backup backup 533503 Dec 12 00:08 mattermost_221212_000808.sql
-rw-r--r--. 1 backup backup 106184 Dec 12 00:08 mattermost_221212_000808.tar.gz

[scooby@db backup]$ tail mattermost_221212_000808.sql

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-12  0:08:08
```

- Création d'un service avec le script

```bash
[scooby@db ~]$ cd /etc/systemd/system

[scooby@db system]$ sudo vim save.service

[scooby@db system]$ cat save.service
[Unit]
Description=Dump the mattermost database

[Service]
ExecStart=/srv/save.sh
Type=oneshot
User=backup
WorkingDirectory=/srv/backup

[Install]
WantedBy=multi-user.target

[scooby@db system]$ sudo systemctl daemon-reload

[scooby@db system]$ sudo systemctl start save

[scooby@db system]$ ls /srv/backup/
mattermost_221212_000808.sql     mattermost_221212_002447.tar.gz
mattermost_221212_000808.tar.gz

[scooby@db system]$ sudo systemctl enable save
```

- Verifcation que les fichiers sont bien partagés

```bash
[scooby@restore ~]$ ls -l /srv/nfs_shares/shared/
total 732
-rw-r--r--. 1 backup backup 533503 Dec 12 00:08 mattermost_221212_000808.sql
-rw-r--r--. 1 backup backup 106184 Dec 12 00:08 mattermost_221212_000808.tar.gz
-rw-r--r--. 1 backup backup 106181 Dec 12 00:24 mattermost_221212_002447.tar.gz
```