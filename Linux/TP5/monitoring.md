# **Monitoring**

| Machine               | IP            | Service                    |
|-----------------------|---------------|----------------------------|
| `chat.mattermost.srv` | `10.107.1.11` | mattermost                 |
| `db.mattermost.srv`   | `10.107.1.12` | Base de donnée (mariaDB)   |


- Installation sur `chat.mattermost.srv`
```bash
[scooby@chat ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
--2022-12-06 23:30:08--  https://my-netdata.io/kickstart.sh
Resolving my-netdata.io (my-netdata.io)... 188.114.97.2, 188.114.96.2, 2a06:98c1:3121::2, ...
Connecting to my-netdata.io (my-netdata.io)|188.114.97.2|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [application/octet-stream]
Saving to: ‘/tmp/netdata-kickstart.sh’
[...]
Installed:
  libuv-1:1.42.0-1.el9.x86_64  netdata-1.37.0.19.nightly-1.el9.x86_64  protobuf-3.14.0-13.el9.x86_64

Complete!
 OK

Tue Dec  6 23:39:48 CET 2022 : INFO: netdata-updater.sh:  Auto-updating has been ENABLED through cron, updater script linked to /etc/cron.daily/netdata-updater\n
Tue Dec  6 23:39:48 CET 2022 : INFO: netdata-updater.sh:  If the update process fails and you have email notifications set up correctly for cron on this system, you should receive an email notification of the failure.
Tue Dec  6 23:39:48 CET 2022 : INFO: netdata-updater.sh:  Successful updates will not send an email.
Successfully installed the Netdata Agent.

Official documentation can be found online at https://learn.netdata.cloud/docs/.

Looking to monitor all of your infrastructure with Netdata? Check out Netdata Cloud at https://app.netdata.cloud.

Join our community and connect with us on:
  - GitHub: https://github.com/netdata/netdata/discussions
  - Discord: https://discord.gg/5ygS846fR6
  - Our community forums: https://community.netdata.cloud/
```

- Lancement du service `Netdata`
```bash
[scooby@chat ~]$ sudo systemctl start netdata
[sudo] password for scooby:
[scooby@chat ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-12-06 23:39:48 CET; 36min ago
   Main PID: 1336 (netdata)
      Tasks: 60 (limit: 5905)
     Memory: 115.8M
        CPU: 34.399s
     CGroup: /system.slice/netdata.service
             ├─1336 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
             ├─1341 /usr/sbin/netdata --special-spawn-server
             ├─1517 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
             ├─1521 /usr/libexec/netdata/plugins.d/go.d.plugin 1
             └─1526 /usr/libexec/netdata/plugins.d/apps.plugin 1

Dec 06 23:39:48 chat.mattermost.srv netdata[1336]: 2022-12-06 23:39:48: netdata INFO  : MAIN : CONFIG>
Dec 06 23:39:48 chat.mattermost.srv netdata[1336]: Found 0 legacy dbengines, setting multidb diskspac>
Dec 06 23:39:48 chat.mattermost.srv netdata[1336]: Created file '/var/lib/netdata/dbengine_multihost_>
Dec 06 23:39:48 chat.mattermost.srv netdata[1336]: 2022-12-06 23:39:48: netdata INFO  : MAIN : Found >
Dec 06 23:39:48 chat.mattermost.srv netdata[1336]: 2022-12-06 23:39:48: netdata INFO  : MAIN : Create>
Dec 06 23:39:48 chat.mattermost.srv ebpf.plugin[1525]: Does not have a configuration file inside `/et>
Dec 06 23:39:48 chat.mattermost.srv ebpf.plugin[1525]: Name resolution is disabled, collector will no>
Dec 06 23:39:48 chat.mattermost.srv ebpf.plugin[1525]: The network value of CIDR 127.0.0.1/8 was upda>
Dec 06 23:39:48 chat.mattermost.srv ebpf.plugin[1525]: Cannot read process groups configuration file >
Dec 06 23:39:48 chat.mattermost.srv ebpf.plugin[1525]: ebpf.plugin should either run as root (now run>'

[scooby@chat ~]$ sudo systemctl enable netdata
```

- Configuration `Firewalld`
```bash
[scooby@chat ~]$ sudo ss -laptn | grep netdata
LISTEN 0      4096       127.0.0.1:8125       0.0.0.0:*     users:(("netdata",pid=1336,fd=99))

LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*     users:(("netdata",pid=1336,fd=6))

LISTEN 0      4096           [::1]:8125          [::]:*     users:(("netdata",pid=1336,fd=98))

LISTEN 0      4096            [::]:19999         [::]:*     users:(("netdata",pid=1336,fd=7))

[scooby@chat ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success

[scooby@chat ~]$ sudo firewall-cmd --reload
success

[scooby@chat ~]$ sudo firewall-cmd --list-ports
8065/tcp 19999/tcp
```

- Paramétrage de Netdata pour avoir les alertes sur Discord

```bash
[scooby@chat ~]$ cd /etc/netdata/

[scooby@chat netdata]$ sudo vim health.d/cpu_usage.conf

[scooby@chat netdata]$ sudo netdatacli reload-health

[scooby@chat netdata]$ sudo /etc/netdata/edit-config health_alarm_notify.conf
Copying '/etc/netdata/../../usr/lib/netdata/conf.d//health_alarm_notify.conf' to '/etc/netdata//health_alarm_notify.conf' ...
Editing '/etc/netdata/health_alarm_notify.conf' ...

[1]+  Stopped                 sudo /etc/netdata/edit-config health_alarm_notify.conf
[scooby@chat netdata]$ sudo /etc/netdata/edit-config health_alarm_notify.conf
Editing '/etc/netdata/health_alarm_notify.conf' ...

[scooby@chat netdata]$ sudo netdatacli reload-health
```
## **On effectue la même opération sur `db.mattermost.srv`**

- Test des alertes discord

```bash
[scooby@chat netdata]$ sudo dnf install stress-ng -y
Last metadata expiration check: 0:08:54 ago on Thu Dec  8 11:53:11 2022.
Dependencies resolved.
[...]
Installed:
  Judy-1.0.5-28.el9.x86_64           lksctp-tools-1.0.19-2.el9.x86_64           stress-ng-0.14.00-2.el9.x86_64

Complete!


[scooby@chat netdata]$ sudo stress-ng -c 1 -l 100
```

**Le webhook envoie bien un message dans le salon dédidé aux alertes***

Le lien du [message](https://discord.com/channels/1045070295150641223/1050345756780933160/1050372204237443112)