# **TP5 : Make your own IT Infrastructure**

## Sommaire

- [TP5 : Make your own IT Infrastructure](#tp5--make-your-own-it-infrastructure)
- [Sommaire](#sommaire)
- [I. Installation de l'application](#installation-de-lapplication)
- [II. Base de donnée](#base-de-donnée)
  - [1. Mise en place de la base de donnée](#mise-en-place-de-la-base-de-donnée)
- [III. Améliorations]()
  - [1. Reverse Proxy et HTTPS](./Reverseproxy.md)
  - [2. Monitoring](./monitoring.md)
  - [3. Fail2Ban](./fail2ban.md)
  - [4. Sauvegade Base de donnée](./database-saving.md)

## Installation de l'application

| Machine               | IP            | Service     |
|-----------------------|---------------|-------------|
| `chat.mattermost.srv` | `10.107.1.11` | mattermost  |


- Installation de Mattermost (application de communication textuel)

```bash
[scooby@chat ~]$ sudo dnf install wget
Last metadata expiration check: 0:19:28 ago on Thu Dec  1 11:34:13 2022.
Dependencies resolved.
[...]
Installed:
  wget-1.21.1-7.el9.x86_64

Complete!

[scooby@chat ~]$ wget https://releases.mattermost.com/7.5.1/mattermost-7.5.1-linux-amd64.tar.gz
--2022-12-01 11:55:05--  https://releases.mattermost.com/7.5.1/mattermost-7.5.1-linux-amd64.tar.gz
Resolving releases.mattermost.com (releases.mattermost.com)... 13.224.189.7, 13.224.189.43, 13.224.189.78, ...
Connecting to releases.mattermost.com (releases.mattermost.com)|13.224.189.7|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 346059466 (330M) [application/x-tar]
Saving to: ‘mattermost-7.5.1-linux-amd64.tar.gz’

mattermost-7.5.1-linux-am 100%[===================================>] 330.03M  1.97MB/s    in 3m 0s

2022-12-01 11:58:06 (1.83 MB/s) - ‘mattermost-7.5.1-linux-amd64.tar.gz’ saved [346059466/346059466]

[scooby@chat ~]$ tar -xvzf mattermost*.gz
mattermost/
mattermost/bin/
mattermost/bin/mattermost
mattermost/bin/mmctl
mattermost/logs/
mattermost/prepackaged_plugins/
mattermost/prepackaged_plugins/mattermost-plugin-antivirus-v0.1.2-linux-amd64.tar.gz
mattermost/prepackaged_plugins/mattermost-plugin-antivirus-v0.1.2-linux-amd64.tar.gz.sig
mattermost/prepackaged_plugins/mattermost-plugin-autolink-v1.2.2-linux-amd64.tar.gz
mattermost/prepackaged_plugins/mattermost-plugin-autolink-v1.2.2-linux-amd64.tar.gz.sig
mattermost/prepackaged_plugins/mattermost-plugin-aws-SNS-v1.2.0-linux-amd64.tar.gz
mattermost/prepackaged_plugins/mattermost-plugin-aws-SNS-v1.2.0-linux-amd64.tar.gz.sig
[...]
mattermost/client/main.94bef8661903342eb099.js
mattermost/client/main.94bef8661903342eb099.js.map
mattermost/client/manifest.json
mattermost/client/root.html
mattermost/ENTERPRISE-EDITION-LICENSE.txt
mattermost/NOTICE.txt
mattermost/README.md
mattermost/manifest.txt

[scooby@chat ~]$ sudo mv mattermost /opt

[scooby@chat ~]$ sudo mkdir /opt/mattermost/data

[scooby@chat ~]$ sudo useradd --system --user-group mattermost

[scooby@chat ~]$ sudo chown -R mattermost:mattermost /opt/mattermost

[scooby@chat ~]$ sudo chmod -R g+w /opt/mattermost
```

## Base  de donnée

| Machine             | IP            | Service                    |
|---------------------|---------------|----------------------------|
| `db.mattermost.srv` | `10.107.1.12` | Base de donnée (mariaDB)   |


- Installation de Mariadb

```bash
[scooby@db ~]$ sudo dnf install mariadb
# J'ai oublié de copier les informations de l'installation
```

- On améliore la sécurité de la base de donnée

```bash
[scooby@db ~]$ sudo mysql_secure_installation
```

### Mise en place de la base de donnée

- Connexion à la base de donnée

```bash
[scooby@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

- Création d'un utilisateur dans la DB

```sql
MariaDB [(none)]> CREATE USER 'mmuser'@'10.107.1.11' IDENTIFIED BY 'pewpewpew';
```

- Création de la base de donnée qui sera utilisé par Mattermost

```sql
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS mattermost CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```

- On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer

```sql
MariaDB [(none)]> GRANT ALL PRIVILEGES ON mattermost.* TO 'mmuser'@'10.107.1.11';
```

- Actualisation des privilèges

```sql
MariaDB [(none)]> FLUSH PRIVILEGES;
```

- Verification de la base de donnée 

```sql
MariaDB [(none)]> SHOW DATABASES;
+----------------------+
| Database             |
+----------------------+
| information_schema   |
| mattermost           |
| mysql                |
| performance_schema   |
+----------------------+
```

- Connexion entre la base de donnés et le service Mattermost

```bash
[scooby@chat ~]$ sudo vim /opt/mattermost/config/config.json

[scooby@chat ~]$ sudo cat /opt/mattermost/config/config.json
[...]
"SqlSettings": {
    "DriverName": "mysql",
    "DataSource": "mmuser:pewpewpew@tcp(10.107.1.12:3306)/mattermost?charset=utf8mb4,utf8&writeTimeout=30s",
[...]
```

- Lancement de Mattermost

```bash
[scooby@chat ~]$ cd /opt/mattermost/

[scooby@chat mattermost]$ sudo -u mattermost bin/mattermost
{"timestamp":"2022-12-05 11:46:25.147 +01:00","level":"info","msg":"Server is initializing...","caller":"platform/service.go:157","go_version":"go1.18.1"}
{"timestamp":"2022-12-05 11:46:25.148 +01:00","level":"info","msg":"Pinging SQL","caller":"sqlstore/store.go:230","database":"master"}
[...]
{"timestamp":"2022-12-05 11:46:29.815 +01:00","level":"info","msg":"Starting Server...","caller":"app/server.go:913"}
{"timestamp":"2022-12-05 11:46:29.815 +01:00","level":"info","msg":"Server is listening on [::]:8065","caller":"app/server.go:985","address":"[::]:8065"}
```

Impossible de pouvoir curl parce que le serveur se lance au premier plan

- Mise en place de Mattermost en tant que service
```bash
[scooby@reverseproxy ~]$ sudo vim /lib/systemd/system/mattermost.service

[scooby@reverseproxy ~]$ sudo cat /lib/systemd/system/mattermost.service
[Unit]
Description=Mattermost
After=network.target
[Service]
Type=notify
ExecStart=/opt/mattermost/bin/mattermost
TimeoutStartSec=3600
KillMode=mixed
Restart=always
RestartSec=10
WorkingDirectory=/opt/mattermost
User=mattermost
Group=mattermost
LimitNOFILE=49152
[Install]
WantedBy=multi-user.target

[scooby@reverseproxy ~]$ sudo systemctl daemon-reload


[scooby@reverseproxy ~]$ sudo systemctl status mattermost.service
○ mattermost.service - Mattermost
     Loaded: loaded (/etc/systemd/system/mattermost.service; disabled; vendor preset: disabled)
     Active: inactive (dead)

[scooby@chat ~]$ curl 10.107.1.11:8065
<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"><meta name="robots" content="noindex, nofollow"><meta name="referrer" content="no-referrer"><title>Mattermost</title><meta name="mobile-web-app-capable" content="yes"><meta name="application-name" content="Mattermost"><meta name="format-detection" content="telephone=no">
[...]
<div class="loading__content"><div class="round round-1"></div><div class="round round-2"></div><div class="round round-3"></div></div></div></div><div id="root-portal"></div><noscript>To use Mattermost, please enable JavaScript.</noscript></body></html>
```