# TP2 : Gestion de service

# Sommaire

- [TP2 : Gestion de service](#tp2--gestion-de-service)
- [Sommaire](#sommaire)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro blabla](#1-intro-blabla)
  - [2. Setup](#2-setup)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web et NextCloud](#b-serveur-web-et-nextcloud)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service     |
|-----------------|---------------|-------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web |

🌞 **Installer le serveur Apache**

- paquet `httpd`
```bash
[scooby@web ~]$ sudo dnf install httpd
Last metadata expiration check: 3:02:45 ago on Thu Nov 17 16:26:49 2022.
Dependencies resolved.
[...]
Complete!
```

- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`
```bash
[scooby@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
```

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le
```bash
[scooby@web ~]$ sudo systemctl status httpd
○ httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
     Active: inactive (dead)
       Docs: man:httpd.service(8)

[scooby@web ~]$ sudo systemctl start httpd

[scooby@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-17 23:27:55 CET; 3s ago
       Docs: man:httpd.service(8)
   Main PID: 10903 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 5896)
     Memory: 35.1M
        CPU: 49ms
     CGroup: /system.slice/httpd.service
             ├─10903 /usr/sbin/httpd -DFOREGROUND
             ├─10904 /usr/sbin/httpd -DFOREGROUND
             ├─10905 /usr/sbin/httpd -DFOREGROUND
             ├─10906 /usr/sbin/httpd -DFOREGROUND
             └─10907 /usr/sbin/httpd -DFOREGROUND

Nov 17 23:27:55 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 17 23:27:55 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 17 23:27:55 web.tp2.linux httpd[10903]: Server configured, listening on: port 80
  
```
  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
  ```bash
[scooby@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
  ```
  - ouvrez le port firewall nécessaire
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache
```bash
[scooby@web ~]$ sudo ss -laptn |grep httpd
LISTEN 0      511                *:80              *:*     users:(("httpd",pid=736,fd=4),("httpd",pid=735,fd=4),("httpd",pid=734,fd=4),("httpd",pid=712,fd=4))
```

🌞 **TEST**

- vérifier que le service est démarré
```bash
[scooby@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-17 23:35:43 CET; 1h 42min ago
       Docs: man:httpd.service(8)
   Main PID: 712 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 5896)
     Memory: 28.1M
        CPU: 2.387s
     CGroup: /system.slice/httpd.service
             ├─712 /usr/sbin/httpd -DFOREGROUND
             ├─731 /usr/sbin/httpd -DFOREGROUND
             ├─734 /usr/sbin/httpd -DFOREGROUND
             ├─735 /usr/sbin/httpd -DFOREGROUND
             └─736 /usr/sbin/httpd -DFOREGROUND

Nov 17 23:35:43 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 17 23:35:43 web.tp2.linux httpd[712]: AH00558: httpd: Could not reliably determine the server's fully qualified dom>
Nov 17 23:35:43 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 17 23:35:43 web.tp2.linux httpd[712]: Server configured, listening on: port 80
```

- vérifier qu'il est configuré pour démarrer automatiquement
```bash
[scooby@web ~]$ sudo systemctl is-enabled httpd
enabled
```
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```bash
[scooby@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
      [...]
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

```bash
PS C:\Users\glaro> curl 10.102.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
          [...]
              <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>
  </body>
</html>
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
```bash
[scooby@web ~]$ cat /etc/systemd/system/multi-user.target.wants/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé
```bash
[scooby@web ~]$ cat /etc/httpd/conf/httpd.conf |grep User
User apache
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```bash
[scooby@web ~]$ ps -ef | grep apache
apache       731     712  0 09:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       734     712  0 09:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       735     712  0 09:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       736     712  0 09:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
scooby      1383    1292  0 10:21 pts/0    00:00:00 grep --color=auto apache
```

- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf

```bash
[scooby@web ~]$ sudo ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Nov 17 19:48 .
drwxr-xr-x. 78 root root 4096 Nov 17 19:51 ..
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```

🌞 **Changer l'utilisateur utilisé par Apache**

- créez un nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant
    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut
    ```bash
    [scooby@web ~]$ sudo adduser toto

    [scooby@web ~]$ sudo passwd toto
    Changing password for user toto.
    New password:
    BAD PASSWORD: The password is shorter than 8 characters
    Retype new password:
    passwd: all authentication tokens updated successfully.

    [scooby@web ~]$ sudo usermod -aG apache toto

    [scooby@web ~]$ groups toto
    toto : toto apache
    ```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```bash
[scooby@web ~]$ sudo vim /etc/httpd/conf/httpd.conf

[scooby@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User toto
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```

- redémarrez Apache
```bash
[scooby@web ~]$ sudo systemctl restart httpd
```
- utilisez une commande `ps` pour vérifier que le changement a pris effet
```bash
[scooby@web ~]$ ps -ef | grep httpd
root        1518       1  0 11:22 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        1519    1518  0 11:22 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        1520    1518  0 11:22 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        1521    1518  0 11:22 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        1522    1518  0 11:22 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
scooby      1749    1292  0 11:24 pts/0    00:00:00 grep --color=auto httpd
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix
```bash
[scooby@web ~]$ sudo vim /etc/httpd/conf/httpd.conf

[scooby@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep Listen
Listen 8080
```
- ouvrez ce nouveau port dans le firewall, et fermez l'ancien
```bash
[scooby@web ~]$ sudo firewall-cmd --remove-port=80/tcp
success

[scooby@web ~]$ sudo firewall-cmd --add-port=8080/tcp
success

[scooby@web ~]$ sudo firewall-cmd --list-port
22/tcp 8080/tcp
```
- redémarrez Apache
```bash
[scooby@web ~]$ sudo systemctl restart httpd
```
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```bash
[scooby@web ~]$ sudo ss -laptn |grep httpd
LISTEN 0      511                *:8080            *:*     users:(("httpd",pid=1786,fd=4),("httpd",pid=1785,fd=4),("httpd",pid=1784,fd=4),("httpd",pid=1780,fd=4))
```
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```bash
[scooby@web ~]$ curl 10.102.1.11:8080
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
      [...]
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port
```bash
PS C:\Users\glaro> curl 10.102.1.11:8080
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    [...]
    <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

📁 **[Httpd Conf Files](./Files/httpd.conf)**

# II. Une stack web plus avancée

⚠⚠⚠ **Réinitialiser votre conf Apache avant de continuer** ⚠⚠⚠  
En particulier :

- reprendre le port par défaut
- reprendre l'utilisateur par défaut

## 1. Intro blabla

**Le serveur web `web.tp2.linux` sera le serveur qui accueillera les clients.** C'est sur son IP que les clients devront aller pour visiter le site web.  

**Le service de base de données `db.tp2.linux` sera uniquement accessible depuis `web.tp2.linux`.** Les clients ne pourront pas y accéder. Le serveur de base de données stocke les infos nécessaires au serveur web, pour le bon fonctionnement du site web.

---

Bon le but de ce TP est juste de s'exercer à faire tourner des services, un serveur + sa base de données, c'est un peu le cas d'école. J'ai pas envie d'aller deep dans la conf de l'un ou de l'autre avec vous pour le moment, on va se contenter d'une conf minimale.

Je vais pas vous demander de coder une application, et cette fois on se contentera pas d'un simple `index.html` tout moche et on va se mettre dans la peau de l'admin qui se retrouve avec une application à faire tourner. **On va faire tourner un [NextCloud](https://nextcloud.com/).**

En plus c'est utile comme truc : c'est un p'tit serveur pour héberger ses fichiers via une WebUI, style Google Drive. Mais on l'héberge nous-mêmes :)

---

Le flow va être le suivant :

➜ **on prépare d'abord la base de données**, avant de setup NextCloud

- comme ça il aura plus qu'à s'y connecter
- ce sera sur une nouvelle machine `db.tp2.linux`
- il faudra installer le service de base de données, puis lancer le service
- on pourra alors créer, au sein du service de base de données, le nécessaire pour NextCloud

➜ **ensuite on met en place NextCloud**

- on réutilise la machine précédente avec Apache déjà installé, ce sera toujours Apache qui accueillera les requêtes des clients
- mais plutôt que de retourner une bête page HTML, NextCloud traitera la requête
- NextCloud, c'est codé en PHP, il faudra donc **installer une version de PHP précise** sur la machine
- on va donc : install PHP, configurer Apache, récupérer un `.zip` de NextCloud, et l'extraire au bon endroit !

![NextCloud install](./pics/nc_install.png)

## 2. Setup

🖥️ **VM db.tp2.linux**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

| Machines        | IP            | Service                 |
|-----------------|---------------|-------------------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données |

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/)
- je veux dans le rendu **toutes** les commandes réalisées
```bash
[scooby@db ~]$ sudo dnf install mariadb-server
Rocky Linux 9 - BaseOS                                                                  1.8 MB/s | 1.7 MB     00:00
Rocky Linux 9 - AppStream                                                               2.5 MB/s | 6.0 MB     00:02
Rocky Linux 9 - Extras                                                                   15 kB/s | 6.6 kB     00:00
Dependencies resolved.
[...]
  python3-audit-3.0.7-101.el9_0.2.x86_64                    python3-libsemanage-3.3-2.el9.x86_64
  python3-policycoreutils-3.3-6.el9_0.noarch                python3-setools-4.4.0-4.el9.x86_64
  python3-setuptools-53.0.0-10.el9.noarch

Complete!

[scooby@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[scooby@db ~]$ sudo systemctl start mariadb

[scooby@db ~]$ mysql_secure_installation
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n]
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n]
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n]
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`
```bash
[scooby@db ~]$ sudo ss -laptn |grep mariadb
LISTEN 0      80                 *:3306            *:*     users:(("mariadbd",pid=12343,fd=19))
```
  - il sera nécessaire de l'ouvrir dans le firewall
  ```bash
  [scooby@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
  success

  [scooby@db ~]$ sudo firewall-cmd --list-port
  3306/tcp

  [scooby@db ~]$ sudo systemctl restart firewalld
  ```

> La doc vous fait exécuter la commande `mysql_secure_installation` c'est un bon réflexe pour renforcer la base qui a une configuration un peu *chillax* à l'install.

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`
```bash
[scooby@db ~]$ sudo mysql -u root -p
Enter password:
```
```sql
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 22
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - utilisez la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
    - si vous ne l'avez pas, installez-là
    - vous pouvez déterminer dans quel paquet est disponible la commande `mysql` en saisissant `dnf provides mysql`
```bash
#Installation de mysql
[scooby@web ~]$ sudo dnf install mysql
[sudo] password for scooby:
Rocky Linux 9 - BaseOS                                                                                  3.7 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                                               7.6 kB/s | 3.6 kB     00:00
Rocky Linux 9 - Extras                                                                                  5.4 kB/s | 2.9 kB     00:00
Dependencies resolved.
[...]
Installed:
  mariadb-connector-c-config-3.2.6-1.el9_0.noarch         mysql-8.0.30-3.el9_0.x86_64         mysql-common-8.0.30-3.el9_0.x86_64

Complete!

# Connexion a la base de donnée depuis "web"
[scooby@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.5.5-10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```
    
- **donc vous devez effectuer une commande `mysql` sur `web.tp2.linux`**
- une fois connecté à la base, utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
SHOW DATABASES;
USE <DATABASE_NAME>;
SHOW TABLES;
```

```sql
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.01 sec)

mysql> USE nextcloud;
Database changed

mysql> SHOW TABLES;
Empty set (0.00 sec)
```

🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

- vous ne pourrez pas utiliser l'utilisateur `nextcloud` de la base pour effectuer cette opération : il n'a pas les droits
- il faudra donc vous reconnectez localement à la base en utilisant l'utilisateur `root`

```bash
[scooby@db ~]$ mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 6
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SELECT user FROM mysql.user;
+-------------+
| User        |
+-------------+
| nextcloud   |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
4 rows in set (0.001 sec)
```

> Les utilisateurs de la base de données sont différents des utilisateurs du système Rocky Linux qui porte la base. Les utilisateurs de la base définissent des identifiants utilisés pour se connecter à la base afin d'y voir ou d'y modifier des données.

Une fois qu'on s'est assurés qu'on peut se co au service de base de données depuis `web.tp2.linux`, on peut continuer.

### B. Serveur Web et NextCloud

⚠️⚠️⚠️ **N'OUBLIEZ PAS de réinitialiser votre conf Apache avant de continuer. En particulier, remettez le port et le user par défaut.**

🌞 **Install de PHP**

```bash
# On ajoute le dépôt CRB
[scooby@web ~]$ sudo dnf config-manager --set-enabled crb
[sudo] password for scooby:

# On ajoute le dépôt REMI
[scooby@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
Rocky Linux 9 - BaseOS                                                                  6.9 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                               6.5 kB/s | 3.6 kB     00:00
Rocky Linux 9 - CRB                                                                     1.4 MB/s | 1.9 MB     00:01
Rocky Linux 9 - Extras                                                                  6.3 kB/s | 2.9 kB     00:00
remi-release-9.rpm                                                                      247 kB/s |  25 kB     00:00
Dependencies resolved.
[...]
Installed:
  epel-release-9-4.el9.noarch         remi-release-9.0-6.el9.remi.noarch         yum-utils-4.0.24-4.el9_0.noarch

Complete!

# On liste les versions de PHP dispos, au passage on va pouvoir accepter les clés du dépôt REMI
[scooby@web ~]$ dnf module list php
Extra Packages for Enterprise Linux 9 - x86_64                                          2.2 MB/s |  11 MB     00:05
Remi's Modular repository for Enterprise Linux 9 - x86_64                               2.6 kB/s | 833  B     00:00
Remi's Modular repository for Enterprise Linux 9 - x86_64                               3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x478F8947:
 Userid     : "Remi's RPM repository (https://rpms.remirepo.net/) <remi@remirepo.net>"
 Fingerprint: B1AB F71E 14C9 D748 97E1 98A8 B195 27F1 478F 8947
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el9
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 9 - x86_64                               1.4 MB/s | 754 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 9 - x86_64                              3.0 kB/s | 833  B     00:00
Safe Remi's RPM repository for Enterprise Linux 9 - x86_64                              3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x478F8947:
 Userid     : "Remi's RPM repository (https://rpms.remirepo.net/) <remi@remirepo.net>"
 Fingerprint: B1AB F71E 14C9 D748 97E1 98A8 B195 27F1 478F 8947
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el9
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 9 - x86_64                              1.7 MB/s | 874 kB     00:00
Rocky Linux 9 - BaseOS                                                                  1.5 MB/s | 1.7 MB     00:01
Rocky Linux 9 - AppStream                                                               2.4 MB/s | 6.0 MB     00:02
Rocky Linux 9 - CRB                                                                     1.5 MB/s | 1.9 MB     00:01
Rocky Linux 9 - Extras                                                                   12 kB/s | 6.6 kB     00:00
Remi's Modular repository for Enterprise Linux 9 - x86_64
Name                Stream                 Profiles                                 Summary
php                 remi-7.4               common [d], devel, minimal               PHP scripting language
php                 remi-8.0               common [d], devel, minimal               PHP scripting language
php                 remi-8.1               common [d], devel, minimal               PHP scripting language
php                 remi-8.2               common [d], devel, minimal               PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled"
# On active le dépôt REMI pour récupérer une version spécifique de PHP, celle recommandée par la doc de NextCloud
[scooby@web ~]$ sudo dnf module enable php:remi-8.1 -y
[sudo] password for scooby:
Extra Packages for Enterprise Linux 9 - x86_64                                          2.8 MB/s |  11 MB     00:04
Remi's Modular repository for Enterprise Linux 9 - x86_64                               2.7 kB/s | 833  B     00:00
Remi's Modular repository for Enterprise Linux 9 - x86_64                               3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x478F8947:
 Userid     : "Remi's RPM repository (https://rpms.remirepo.net/) <remi@remirepo.net>"
 Fingerprint: B1AB F71E 14C9 D748 97E1 98A8 B195 27F1 478F 8947
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el9
Remi's Modular repository for Enterprise Linux 9 - x86_64                               1.5 MB/s | 754 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 9 - x86_64                              3.0 kB/s | 833  B     00:00
Safe Remi's RPM repository for Enterprise Linux 9 - x86_64                              3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x478F8947:
 Userid     : "Remi's RPM repository (https://rpms.remirepo.net/) <remi@remirepo.net>"
 Fingerprint: B1AB F71E 14C9 D748 97E1 98A8 B195 27F1 478F 8947
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el9
Safe Remi's RPM repository for Enterprise Linux 9 - x86_64                              1.7 MB/s | 874 kB     00:00
Last metadata expiration check: 0:00:01 ago on Mon Nov 21 09:48:18 2022.
Dependencies resolved.
========================================================================================================================
 Package                     Architecture               Version                       Repository                   Size
========================================================================================================================
Enabling module streams:
 php                                                    remi-8.1

Transaction Summary
========================================================================================================================

Complete!"

# Eeeet enfin, on installe la bonne version de PHP : 8.1
[scooby@web ~]$ sudo dnf install -y php81-php
Last metadata expiration check: 0:03:49 ago on Mon Nov 21 09:48:18 2022.
Dependencies resolved.
[...]
Installed:
  checkpolicy-3.3-1.el9.x86_64                             environment-modules-5.0.1-1.el9.x86_64
  libsodium-1.0.18-8.el9.x86_64                            libxslt-1.1.34-9.el9.x86_64
  oniguruma5php-6.9.8-1.el9.remi.x86_64                    php81-php-8.1.12-1.el9.remi.x86_64
  php81-php-cli-8.1.12-1.el9.remi.x86_64                   php81-php-common-8.1.12-1.el9.remi.x86_64
  php81-php-fpm-8.1.12-1.el9.remi.x86_64                   php81-php-mbstring-8.1.12-1.el9.remi.x86_64
  php81-php-opcache-8.1.12-1.el9.remi.x86_64               php81-php-pdo-8.1.12-1.el9.remi.x86_64
  php81-php-sodium-8.1.12-1.el9.remi.x86_64                php81-php-xml-8.1.12-1.el9.remi.x86_64
  php81-runtime-8.1-2.el9.remi.x86_64                      policycoreutils-python-utils-3.3-6.el9_0.noarch
  python3-audit-3.0.7-101.el9_0.2.x86_64                   python3-libsemanage-3.3-2.el9.x86_64
  python3-policycoreutils-3.3-6.el9_0.noarch               python3-setools-4.4.0-4.el9.x86_64
  python3-setuptools-53.0.0-10.el9.noarch                  scl-utils-1:2.0.3-2.el9.x86_64
  tcl-1:8.6.10-6.el9.x86_64

Complete!
```

🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```bash
[scooby@web ~]$ curl -SLO https://rpmfind.net/linux/opensuse/tumbleweed/repo/oss/x86_64/libhwy1-1.0.2-2.1.x86_64.rpm
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  362k  100  362k    0     0   690k      0 --:--:-- --:--:-- --:--:--  688k

[scooby@web ~]$ sudo rpm -ivh libhwy1-1.0.2-2.1.x86_64.rpm
[sudo] password for scooby:
warning: libhwy1-1.0.2-2.1.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID 3dbdc284: NOKEY
Verifying...                          ################################# [100%]
Preparing...                          ################################# [100%]
Updating / installing...
   1:libhwy1-1.0.2-2.1                ################################# [100%]

[scooby@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
Rocky Linux 9 - BaseOS                                                                  7.5 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                               8.3 kB/s | 3.6 kB     00:00
Rocky Linux 9 - CRB                                                                     8.7 kB/s | 3.6 kB     00:00
Rocky Linux 9 - Extras                                                                  7.4 kB/s | 2.9 kB     00:00
Package libxml2-2.9.13-1.el9_0.1.x86_64 is already installed.
Package openssl-1:3.0.1-43.el9_0.x86_64 is already installed.
Package php81-php-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-mbstring-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-xml-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-pdo-8.1.12-1.el9.remi.x86_64 is already installed.
Dependencies resolved.
[...]
Installed:
  dejavu-sans-fonts-2.37-18.el9.noarch                        fontconfig-2.13.94-2.el9.x86_64
  fonts-filesystem-1:2.0.5-7.el9.1.noarch                     fribidi-1.0.10-6.el9.x86_64
  gd3php-2.3.3-9.el9.remi.x86_64                              gdk-pixbuf2-2.42.6-2.el9.x86_64
  jbigkit-libs-2.1-23.el9.x86_64                              jxl-pixbuf-loader-0.7.0-1.el9.x86_64
  langpacks-core-font-en-3.0-16.el9.noarch                    libX11-1.7.0-7.el9.x86_64
  libX11-common-1.7.0-7.el9.noarch                            libXau-1.0.9-8.el9.x86_64
  libXpm-3.5.13-7.el9.x86_64                                  libaom-3.5.0-2.el9.x86_64
  libavif-0.11.1-0.x86_64                                     libdav1d-1.0.0-2.el9.x86_64
  libicu71-71.1-2.el9.remi.x86_64                             libimagequant-2.17.0-1.el9.x86_64
  libjpeg-turbo-2.0.90-5.el9.x86_64                           libjxl-0.7.0-1.el9.x86_64
  libraqm-0.8.0-1.el9.x86_64                                  libtiff-4.2.0-3.el9.x86_64
  libvmaf-2.3.0-2.el9.x86_64                                  libwebp-1.2.0-3.el9.x86_64
  libxcb-1.13.1-9.el9.x86_64                                  php81-php-bcmath-8.1.12-1.el9.remi.x86_64
  php81-php-gd-8.1.12-1.el9.remi.x86_64                       php81-php-gmp-8.1.12-1.el9.remi.x86_64
  php81-php-intl-8.1.12-1.el9.remi.x86_64                     php81-php-mysqlnd-8.1.12-1.el9.remi.x86_64
  php81-php-pecl-zip-1.21.1-1.el9.remi.x86_64                 php81-php-process-8.1.12-1.el9.remi.x86_64
  remi-libzip-1.9.2-3.el9.remi.x86_64                         shared-mime-info-2.1-4.el9.x86_64
  svt-av1-libs-0.9.0-1.el9.x86_64                             xml-common-0.6.3-58.el9.noarch

Complete!
```

🌞 **Récupérer NextCloud**

- créez le dossier `/var/www/tp2_nextcloud/`
```bash
[scooby@web ~]$ sudo mkdir /var/www/tp2_nextcloud/
```
  - ce sera notre *racine web* (ou *webroot*)
  - l'endroit où le site est stocké quoi, on y trouvera un `index.html` et un tas d'autres marde, tout ce qui constitue NextClo :D
- récupérer le fichier suivant avec une commande `curl` ou `wget` : https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
```bash
[scooby@web ~]$ curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip --output nextcloud.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  168M  100  168M    0     0   989k      0  0:02:53  0:02:53 --:--:-- 1009k
```
- extrayez tout son contenu dans le dossier `/var/www/tp2_nextcloud/` en utilisant la commande `unzip`
  - installez la commande `unzip` si nécessaire
  ```bash
  [scooby@web ~]$ sudo dnf install unzip
  [sudo] password for scooby:
  Last metadata expiration check: 0:18:26 ago on Mon Nov 21 11:31:47 2022.
  Dependencies resolved.
  [...]
  Installed:
  unzip-6.0-56.el9.x86_64

  Complete!
  ```
  - vous pouvez extraire puis déplacer ensuite, vous prenez pas la tête
  ```bash
  [scooby@web ~]$ ls
  nextcloud  nextcloud.zip

  [scooby@web ~]$ sudo mv nextcloud/* /var/www/tp2_nextcloud/

  [scooby@web ~]$ ls
  nextcloud  nextcloud.zip

  [scooby@web ~]$ ls nextcloud

  [scooby@web ~]$ rm -rf nextcloud

  [scooby@web ~]$ ls
  nextcloud.zip

  [scooby@web ~]$ rm -rf nextcloud.zip

  [scooby@web ~]$ ls
  ```
  - contrôlez que le fichier `/var/www/tp2_nextcloud/index.html` existe pour vérifier que tout est en place
  ```bash
  [scooby@web ~]$ ls /var/www/tp2_nextcloud/ | grep index
  index.html
  index.php
  ```
- assurez-vous que le dossier `/var/www/tp2_nextcloud/` et tout son contenu appartient à l'utilisateur qui exécute le service Apache
```bash
[scooby@web ~]$ ls -al /var/www/tp2_nextcloud/
total 132
drwxr-xr-x. 14 root   root    4096 Nov 21 12:29 .
drwxr-xr-x.  5 root   root      54 Nov 21 11:31 ..
drwxr-xr-x. 47 scooby scooby  4096 Oct  6 14:47 3rdparty
-rw-r--r--.  1 scooby scooby 19327 Oct  6 14:42 AUTHORS
-rw-r--r--.  1 scooby scooby 34520 Oct  6 14:42 COPYING
drwxr-xr-x. 50 scooby scooby  4096 Oct  6 14:44 apps
drwxr-xr-x.  2 scooby scooby    67 Oct  6 14:47 config
-rw-r--r--.  1 scooby scooby  4095 Oct  6 14:42 console.php
drwxr-xr-x. 23 scooby scooby  4096 Oct  6 14:47 core
-rw-r--r--.  1 scooby scooby  6317 Oct  6 14:42 cron.php
drwxr-xr-x.  2 scooby scooby  8192 Oct  6 14:42 dist
-rw-r--r--.  1 scooby scooby   156 Oct  6 14:42 index.html
-rw-r--r--.  1 scooby scooby  3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 scooby scooby   125 Oct  6 14:42 lib
-rw-r--r--.  1 scooby scooby   283 Oct  6 14:42 occ
drwxr-xr-x.  2 scooby scooby    23 Oct  6 14:42 ocm-provider
drwxr-xr-x.  2 scooby scooby    55 Oct  6 14:42 ocs
drwxr-xr-x.  2 scooby scooby    23 Oct  6 14:42 ocs-provider
-rw-r--r--.  1 scooby scooby  3139 Oct  6 14:42 public.php
-rw-r--r--.  1 scooby scooby  5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 scooby scooby   133 Oct  6 14:42 resources
-rw-r--r--.  1 scooby scooby    26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 scooby scooby  2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 scooby scooby    35 Oct  6 14:42 themes
drwxr-xr-x.  2 scooby scooby    43 Oct  6 14:44 updater
-rw-r--r--.  1 scooby scooby   387 Oct  6 14:47 version.php

[scooby@web ~]$ sudo chown -R apache:apache /var/www/tp2_nextcloud/

[scooby@web ~]$ ls -al /var/www/tp2_nextcloud/
total 132
drwxr-xr-x. 14 apache apache  4096 Nov 21 12:29 .
drwxr-xr-x.  5 root   root      54 Nov 21 11:31 ..
drwxr-xr-x. 47 apache apache  4096 Oct  6 14:47 3rdparty
-rw-r--r--.  1 apache apache 19327 Oct  6 14:42 AUTHORS
-rw-r--r--.  1 apache apache 34520 Oct  6 14:42 COPYING
drwxr-xr-x. 50 apache apache  4096 Oct  6 14:44 apps
drwxr-xr-x.  2 apache apache    67 Oct  6 14:47 config
-rw-r--r--.  1 apache apache  4095 Oct  6 14:42 console.php
drwxr-xr-x. 23 apache apache  4096 Oct  6 14:47 core
-rw-r--r--.  1 apache apache  6317 Oct  6 14:42 cron.php
drwxr-xr-x.  2 apache apache  8192 Oct  6 14:42 dist
-rw-r--r--.  1 apache apache   156 Oct  6 14:42 index.html
-rw-r--r--.  1 apache apache  3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 apache apache   125 Oct  6 14:42 lib
-rw-r--r--.  1 apache apache   283 Oct  6 14:42 occ
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocm-provider
drwxr-xr-x.  2 apache apache    55 Oct  6 14:42 ocs
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocs-provider
-rw-r--r--.  1 apache apache  3139 Oct  6 14:42 public.php
-rw-r--r--.  1 apache apache  5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 apache apache   133 Oct  6 14:42 resources
-rw-r--r--.  1 apache apache    26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 apache apache  2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 apache apache    35 Oct  6 14:42 themes
drwxr-xr-x.  2 apache apache    43 Oct  6 14:44 updater
-rw-r--r--.  1 apache apache   387 Oct  6 14:47 version.php
```

> A chaque fois que vous faites ce genre de trucs, assurez-vous que c'est bien ok. Par exemple, vérifiez avec un `ls -al` que tout appartient bien à l'utilisateur qui exécute Apache.

🌞 **Adapter la configuration d'Apache**

- regardez la dernière ligne du fichier de conf d'Apache pour constater qu'il existe une ligne qui inclut d'autres fichiers de conf
```bash
[scooby@web ~]$ tail -n 5 /etc/httpd/conf/httpd.conf

EnableSendfile on

IncludeOptional conf.d/*.conf
```
- créez en conséquence un fichier de configuration qui porte un nom clair et qui contient la configuration suivante :

```apache
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/> 
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

```bash
[scooby@web ~]$ cd /etc/httpd/conf.d/

[scooby@web conf.d]$ ls
README  autoindex.conf  php81-php.conf  userdir.conf  welcome.conf

[scooby@web conf.d]$ sudo vim webroot.com
```

🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf

```bash
[scooby@web ~]$ sudo systemctl restart httpd
```

### C. Finaliser l'installation de NextCloud

➜ **Sur votre PC**

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`
- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
  - c'est possible grâce à la modification de votre fichier `hosts`
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
  - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
  - choisissez "MySQL/MariaDB"
  - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

🌴 **C'est chez vous ici**, baladez vous un peu sur l'interface de NextCloud, faites le tour du propriétaire :)

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL

```bash
[scooby@db ~]$ mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 88
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

#Actuellement 
MariaDB [(none)]> SHOW DATABASES
    -> ;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
4 rows in set (0.002 sec)

#Avant
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.01 sec)

#Donc après l'installation, 2 tables ont été créées
```
