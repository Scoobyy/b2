# TP4 : Conteneurs

Dans ce TP on va aborder plusieurs points autour de la conteneurisation : 

- Docker et son empreinte sur le système
- Manipulation d'images
- `docker-compose`

# Sommaire

- [TP4 : Conteneurs](#tp4--conteneurs)
- [I. Docker](#i-docker)
  - [1. Install](#1-install)
  - [2. Vérifier l'install](#2-vérifier-linstall)
  - [3. Lancement de conteneurs](#3-lancement-de-conteneurs)
- [II. Images](#ii-images)
- [III. `docker-compose`](#iii-docker-compose)
  - [1. Intro](#1-intro)
  - [2. Make your own meow](#2-make-your-own-meow)

# I. Docker

🖥️ Machine **docker1.tp4.linux**

## 1. Install

🌞 **Installer Docker sur la machine**

- en suivant [la doc officielle](https://docs.docker.com/engine/install/)
```bash
[scooby@docker ~]$ sudo dnf install yum-utils -y
Last metadata expiration check: 0:30:16 ago on Thu Nov 24 11:13:06 2022.
Dependencies resolved.
[...]
Installed:
  yum-utils-4.0.24-4.el9_0.noarch

Complete!

[scooby@docker ~]$ sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo

[scooby@docker ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io docker-compose-plugin
Docker CE Stable - x86_64                                                               1.2 kB/s |  12 kB     00:10
Last metadata expiration check: 0:00:05 ago on Thu Nov 24 11:46:14 2022.
Dependencies resolved.
[...]
Installed:
  checkpolicy-3.3-1.el9.x86_64                             container-selinux-3:2.188.0-1.el9_0.noarch
  containerd.io-1.6.10-3.1.el9.x86_64                      docker-ce-3:20.10.21-3.el9.x86_64
  docker-ce-cli-1:20.10.21-3.el9.x86_64                    docker-ce-rootless-extras-20.10.21-3.el9.x86_64
  docker-compose-plugin-2.12.2-3.el9.x86_64                docker-scan-plugin-0.21.0-3.el9.x86_64
  fuse-common-3.10.2-5.el9.0.1.x86_64                      fuse-overlayfs-1.9-1.el9_0.x86_64
  fuse3-3.10.2-5.el9.0.1.x86_64                            fuse3-libs-3.10.2-5.el9.0.1.x86_64
  libslirp-4.4.0-7.el9.x86_64                              policycoreutils-python-utils-3.3-6.el9_0.noarch
  python3-audit-3.0.7-101.el9_0.2.x86_64                   python3-libsemanage-3.3-2.el9.x86_64
  python3-policycoreutils-3.3-6.el9_0.noarch               python3-setools-4.4.0-4.el9.x86_64
  python3-setuptools-53.0.0-10.el9.noarch                  slirp4netns-1.2.0-2.el9_0.x86_64
  tar-2:1.34-3.el9.x86_64

Complete!
```
- démarrer le service `docker` avec une commande `systemctl`
```bash
[scooby@docker ~]$ sudo systemctl start docker

[scooby@docker ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
```

- ajouter votre utilisateur au groupe `docker`
  - cela permet d'utiliser Docker sans avoir besoin de l'identité de `root`
  - avec la commande : `sudo usermod -aG docker $(whoami)`
  - déconnectez-vous puis relancez une session pour que le changement prenne effet
  ```bash
  [scooby@docker ~]$ sudo usermod -aG docker $(whoami)
  
  [scooby@docker ~]$ groups scooby
  scooby : scooby wheel docker
  ```

## 2. Vérifier l'install

➜ **Vérifiez que Docker est actif est disponible en essayant quelques commandes usuelles :**

- Test d'une commande pour voir si docker est bien installé

```bash
[scooby@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

➜ **Explorer un peu le help**, si c'est pas le man :

```bash
$ docker --help
$ docker run --help
$ man docker
```

## 3. Lancement de conteneurs

🌞 **Utiliser la commande `docker run`**

- lancer un conteneur `nginx`
  - l'app NGINX doit avoir un fichier de conf personnalisé
  - l'app NGINX doit servir un fichier `index.html` personnalisé
  - l'application doit être joignable grâce à un partage de ports
  - vous limiterez l'utilisation de la RAM et du CPU de ce conteneur
  - le conteneur devra avoir un nom
  - le processus exécuté par le conteneur doit être un utilisateur de votre choix (pas `root`)

> Tout se fait avec des options de la commande `docker run`.

- Création d'un fichier conf personnalisé pour nginx
```bash
[scooby@docker1 ~]$ mkdir nginx_docker

[scooby@docker1 ~]$ sudo vim nginx_docker/custom.conf

[scooby@docker1 ~]$ cat /var/nginx/conf/custom.conf
server {
  # on définit le port où NGINX écoute dans le conteneur
  listen 9999;

  # on définit le chemin vers la racine web
  # dans ce dossier doit se trouver un fichier index.html
  root /var/nginx/html/;
}
```


- Création du fichier index.html
```bash
[scooby@docker1 ~]$ mkdir nginx_docker/html

[scooby@docker1 ~]$ sudo vim nginx_docker/html/index.html

[scooby@docker1 ~]$ cat nginx_docker/html/index.html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Docker Nginx</title>
</head>
<body>+
    <h1>Bonjour, nginx lancé depuis Docker</h1>

</body>
</html>
```

- Lancement du conteneur
```bash
[scooby@docker1 ~]$ docker run --name web -d -v /home/scooby/nginx_docker/html:/var/nginx/html -v /home/scooby/nginx_docker/custom.conf:/etc/nginx/conf.d/custom.conf -p 1703:9999 -m 512m --cpus=1 --rm nginx
dd0f7961e04322d1defa8f7577f6fffdacef4b617d5014860ad03f801e914e55

[scooby@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                               NAMES
394c4c48bf82   nginx     "/docker-entrypoint.…"   2 seconds ago   Up 2 seconds   80/tcp, 0.0.0.0:1703->9999/tcp :::1703->9999/tcp   web
```

- Ouverture du port sur le firewall
```bash
[scooby@docker1 ~]$ sufo firewall-cmd --add-port=1703/tcp
success

[scooby@docker1 ~]$ sudo firewall-cmd --list-port
1703/tcp
```

- Verification du fonctionement du conteneur
```bash
[scooby@docker1 ~]$ curl 10.101.1.17:1703
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Docker Nginx</title>
</head>
<body>+
    <h1>Bonjour, nginx lancé depuis Docker</h1>

</body>
</html>
```

# II. Images

🌞 **Construire votre propre image**

- image de base (celle que vous voulez : debian, alpine, ubuntu, etc.)
  - une image du Docker Hub
  - qui ne porte aucune application par défaut
- vous ajouterez
  - mise à jour du système
  - installation de Apache (pour les systèmes debian, le serveur Web apache s'appelle `apache2` et non pas `httpd` comme sur Rocky)
  - page d'accueil Apache HTML personnalisée

➜ Pour vous aider, voilà un fichier de conf minimal pour Apache (à positionner dans `/etc/apache2/apache2.conf`) :

```bash
[scooby@docker1 ~]$ mkdir apache_dockerfile

[scooby@docker1 ~]$ cd apache_dockerfile/

[scooby@docker1 apache_dockerfile]$ sudo vim Dockerfile

[scooby@docker1 apache_dockerfile]$ cat Dockerfile
FROM ubuntu
RUN apt update -y
Run apt upgrade -y
RUN apt install apache2 -y
RUN mkdir -p /nginx_docker/html/
RUN mkdir -p /etc/apache2/logs && chmod 755 /etc/apache2/logs
COPY index.html /nginx_docker/html/index.html
COPY toto.conf /etc/apache2/apache2.conf
EXPOSE 80
CMD [ "apache2ctl", "-D", "FOREGROUND" ]

[scooby@docker1 apache_dockerfile]$ docker build . -t apache_image

[scooby@docker1 apache_dockerfile] $ docker run -p 8888:80 -d apache_image

[scooby@docker1 ~]$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                                   NAMES
a7d0a5b19e17   apache_image   "apache2ctl -D FOREG…"   3 minutes ago   Up 3 minutes   0.0.0.0:8888->80/tcp, :::8888->80/tcp   epic_brattain

[scooby@docker1 ~]$ curl localhost:8888
gabriellllllllllll

[scooby@docker1 apache_dockerfile]$ cat index.html
gabriellllllllllll
```

➜ Et aussi, la commande pour lancer Apache à la main sur un système Debian c'est : `apache2 -DFOREGROUND`.

📁 **`Dockerfile`**


# III. `docker-compose`

## 1. Intro

➜ **Installer `docker-compose` sur la machine**

- en suivant [la doc officielle](https://docs.docker.com/compose/install/)

`docker-compose` est un outil qui permet de lancer plusieurs conteneurs en une seule commande.

> En plus d'être pratique, il fournit des fonctionnalités additionnelles, liés au fait qu'il s'occupe à lui tout seul de lancer tous les conteneurs. On peut par exemple demander à un conteneur de ne s'allumer que lorsqu'un autre conteneur est devenu "healthy". Idéal pour lancer une application après sa base de données par exemple.

Le principe de fonctionnement de `docker-compose` :

- on écrit un fichier qui décrit les conteneurs voulus
  - c'est le `docker-compose.yml`
  - tout ce que vous écriviez sur la ligne `docker run` peut être écrit sous la forme d'un `docker-compose.yml`
- on se déplace dans le dossier qui contient le `docker-compose.yml`
- on peut utiliser les commandes `docker-compose` :

```bash
# Allumer les conteneurs définis dans le docker-compose.yml
$ docker-compose up
$ docker-compose up -d

# Eteindre
$ docker-compose down

# Explorer un peu le help, il y a d'autres commandes utiles
$ docker-compose --help
```

La syntaxe du fichier peut par exemple ressembler à :

```yml
version: "3.8"

services:
  db:
    image: mysql:5.7
    restart: always
    ports:
      - '3306:3306'
    volumes:
      - "./db/mysql_files:/var/lib/mysql"
    environment:
      MYSQL_ROOT_PASSWORD: beep
      MYSQL_DATABASE: bip
      MYSQL_USER: bap
      MYSQL_PASSWORD: boop

  nginx:
    image: nginx
    ports:
      - "80:80"
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf:ro
    restart: unless-stopped
```

> Pour connaître les variables d'environnement qu'on peut passer à un conteneur, comme `MYSQL_ROOT_PASSWORD` au dessus, il faut se rendre sur la doc de l'image en question, sur le Docker Hub par exemple.

## 2. Make your own meow

Pour cette partie, vous utiliserez une application à vous que vous avez sous la main.

N'importe quelle app fera le taff, un truc dév en cours, en temps perso, au taff, peu importe.

Peu importe le langage aussi ! Go, Python, PHP (désolé des gros mots), Node (j'ai déjà dit désolé pour les gros mots ?), ou autres.

🌞 **Conteneurisez votre application**

- créer un `Dockerfile` maison qui porte l'application
- créer un `docker-compose.yml` qui permet de lancer votre application
- vous préciserez dans le rendu les instructions pour lancer l'application
  - indiquer la commande `git clone`
  - le `cd` dans le bon dossier
  - la commande `docker build` pour build l'image
  - la commande `docker-compose` pour lancer le(s) conteneur(s)

📁 📁 `app/Dockerfile` et `app/docker-compose.yml`. Je veux un sous-dossier `app/` sur votre dépôt git avec ces deux fichiers dedans :)
